module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	var installedChunks = {
/******/ 		"bundle": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = global["webpackJsonp"] = global["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./app.js","vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/App.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/Header.vue");
/* harmony import */ var _Home__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/Home.vue");
/* harmony import */ var _Searching__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./components/Searching.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




const platform = __webpack_require__("../node_modules/tns-core-modules/platform/platform.js");

const application = __webpack_require__("../node_modules/tns-core-modules/application/application.js");

/* harmony default export */ __webpack_exports__["default"] = ({
  methods: {
    toggleSearch() {
      this.isSearching = !this.isSearching;
    }

  },

  data() {
    return {
      platform: "",
      isSearching: false,
      width: 0,
      height: 0
    };
  },

  mounted() {
    this.width = platform.screen.mainScreen.widthDIPs;
    this.height = platform.screen.mainScreen.heightDIPs;

    if (application.ios) {
      this.platform = "ios";
    } else if (application.android) {
      this.platform = "android";
    }
  },

  components: {
    Header: _Header__WEBPACK_IMPORTED_MODULE_0__["default"],
    Home: _Home__WEBPACK_IMPORTED_MODULE_1__["default"],
    Searching: _Searching__WEBPACK_IMPORTED_MODULE_2__["default"]
  }
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/Card.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Place__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/Place.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    place: {
      type: Object
    },
    index: {
      type: Number
    },
    active: {
      type: Number
    }
  },
  methods: {
    goToPlace(place) {
      console.log("card");
      this.$navigateTo(_Place__WEBPACK_IMPORTED_MODULE_0__["default"], {
        props: {
          place: place
        }
      });
    }

  },

  data() {
    return {};
  }

});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CityOverview.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Place__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/Place.vue");
/* harmony import */ var _Card__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/Card.vue");
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./components/Header.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




const platform = __webpack_require__("../node_modules/tns-core-modules/platform/platform.js");

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    city: {
      type: String,
      default: "Dublin"
    }
  },

  data() {
    return {
      canBack: true,
      page: _Place__WEBPACK_IMPORTED_MODULE_0__["default"],
      width: 0,
      active: 0,
      places: [{
        name: "The Custom House",
        type: "Architecture",
        stars: 4,
        media: "Dublin.jpg"
      }, {
        name: "Christ Church's",
        type: "Architecture",
        stars: 4,
        media: "Cathedral.jpg"
      }, {
        name: "Bord Gáis Energ",
        type: "Architecture",
        stars: 5,
        media: "Theater.jpg"
      }]
    };
  },

  mounted() {
    this.width = platform.screen.mainScreen.widthDIPs;
  },

  methods: {
    onButtonTap() {
      console.log("Button was pressed");
    },

    getScroll(e) {
      this.active = Math.round(e.scrollX / 244);
    }

  },
  components: {
    Header: _Header__WEBPACK_IMPORTED_MODULE_2__["default"],
    Card: _Card__WEBPACK_IMPORTED_MODULE_1__["default"]
  }
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/Header.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    secondary: {
      type: Boolean,
      default: false
    },
    canBack: {
      type: Boolean
    }
  },

  data() {
    return {};
  }

});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/Home.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data() {
    return {};
  },

  methods: {
    toggleSearch() {
      this.$emit("togglesearch");
      console.log("CLICK");
    }

  }
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/Place.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/Header.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    place: {
      type: Object
    }
  },

  data() {
    return {
      canBack: true,
      lorem: `Celeri discursantes optandi nobis nec praedam qui similes parvi aut similes umquam optandi poterat impetraverint si nobis rapiunt nec discursantes qui poterat rapacium nobis milvorum aut volatu nobis impetraverint impetraverint parvi momento volatu impetraverint qui celeri parvi nisi amici similes tamen optandi parvi discursantes impetraverint volatu non discursantes nisi.`
    };
  },

  components: {
    Header: _Header__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/Searching.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CityOverview__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CityOverview.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


const application = __webpack_require__("../node_modules/tns-core-modules/application/application.js");

/* harmony default export */ __webpack_exports__["default"] = ({
  methods: {
    toggleSearch() {
      this.$emit("togglesearch");
    },

    goToCityOverview(city) {
      this.$navigateTo(this.cityOverviewPage, {
        props: {
          city: city
        }
      });
    },

    clearFocus() {
      this.$refs.searchBar.nativeView.dismissSoftInput();
    }

  },

  mounted() {
    //this.searchQuery = "";
    //if (application.android) {
    //setTimeout(() => {
    //this.clearFocus();
    // }, 100);
    // } else if (application.ios) {
    //  setTimeout(() => {
    //      this.searchBarWidth = "100%";
    // }, 500);
    // }
    if (application.ios) {
      this.platform = "ios";
      setTimeout(() => {
        this.searchBarWidth = "100%";
      }, 500);
    } else if (application.android) {
      this.platform = "android";
    }
  },

  computed: {
    filteredCities() {
      if (this.searchQuery !== "") {
        return this.cities.filter(city => city.toLowerCase().includes(this.searchQuery.toLowerCase()));
      }
    }

  },

  data() {
    return {
      platform: "",
      searchBarWidth: "0%",
      cityOverviewPage: _CityOverview__WEBPACK_IMPORTED_MODULE_0__["default"],
      searchQuery: "",
      cities: ["Johannesburg", "Berlin", "Toronto", "Mumbai", "Munich", "Madrid", "Dublin", "Chennai", "India", "Los Angeles", "Miami", "Prague", "Vienna", "Shanghai", "Rome", "Taipei", "Osaka", "Milan", "Amsterdam", "Barcelona", "Istanbul", "Hong Kong", "Kuala Lumpur", "New York", "Seoul", "Tokyo", "Singapore", "Dubai", "Paris", "London", "Bangkok"]
    };
  }

});

/***/ }),

/***/ "../node_modules/css-loader/index.js?!./css/animate.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@charset \"UTF-8\";\r\n\r\n/*!\r\n * animate.css -http://daneden.me/animate\r\n * Version - 3.7.0\r\n * Licensed under the MIT license - http://opensource.org/licenses/MIT\r\n *\r\n * Copyright (c) 2018 Daniel Eden\r\n */\r\n\r\n@-webkit-keyframes bounce {\r\n  from,\r\n  20%,\r\n  53%,\r\n  80%,\r\n  to {\r\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n\r\n  40%,\r\n  43% {\r\n    -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\r\n    animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\r\n    -webkit-transform: translate3d(0, -30px, 0);\r\n    transform: translate3d(0, -30px, 0);\r\n  }\r\n\r\n  70% {\r\n    -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\r\n    animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\r\n    -webkit-transform: translate3d(0, -15px, 0);\r\n    transform: translate3d(0, -15px, 0);\r\n  }\r\n\r\n  90% {\r\n    -webkit-transform: translate3d(0, -4px, 0);\r\n    transform: translate3d(0, -4px, 0);\r\n  }\r\n}\r\n\r\n@keyframes bounce {\r\n  from,\r\n  20%,\r\n  53%,\r\n  80%,\r\n  to {\r\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n\r\n  40%,\r\n  43% {\r\n    -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\r\n    animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\r\n    -webkit-transform: translate3d(0, -30px, 0);\r\n    transform: translate3d(0, -30px, 0);\r\n  }\r\n\r\n  70% {\r\n    -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\r\n    animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\r\n    -webkit-transform: translate3d(0, -15px, 0);\r\n    transform: translate3d(0, -15px, 0);\r\n  }\r\n\r\n  90% {\r\n    -webkit-transform: translate3d(0, -4px, 0);\r\n    transform: translate3d(0, -4px, 0);\r\n  }\r\n}\r\n\r\n.bounce {\r\n  -webkit-animation-name: bounce;\r\n  animation-name: bounce;\r\n  -webkit-transform-origin: center bottom;\r\n  transform-origin: center bottom;\r\n}\r\n\r\n@-webkit-keyframes flash {\r\n  from,\r\n  50%,\r\n  to {\r\n    opacity: 1;\r\n  }\r\n\r\n  25%,\r\n  75% {\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n@keyframes flash {\r\n  from,\r\n  50%,\r\n  to {\r\n    opacity: 1;\r\n  }\r\n\r\n  25%,\r\n  75% {\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n.flash {\r\n  -webkit-animation-name: flash;\r\n  animation-name: flash;\r\n}\r\n\r\n/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */\r\n\r\n@-webkit-keyframes pulse {\r\n  from {\r\n    -webkit-transform: scale3d(1, 1, 1);\r\n    transform: scale3d(1, 1, 1);\r\n  }\r\n\r\n  50% {\r\n    -webkit-transform: scale3d(1.05, 1.05, 1.05);\r\n    transform: scale3d(1.05, 1.05, 1.05);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: scale3d(1, 1, 1);\r\n    transform: scale3d(1, 1, 1);\r\n  }\r\n}\r\n\r\n@keyframes pulse {\r\n  from {\r\n    -webkit-transform: scale3d(1, 1, 1);\r\n    transform: scale3d(1, 1, 1);\r\n  }\r\n\r\n  50% {\r\n    -webkit-transform: scale3d(1.05, 1.05, 1.05);\r\n    transform: scale3d(1.05, 1.05, 1.05);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: scale3d(1, 1, 1);\r\n    transform: scale3d(1, 1, 1);\r\n  }\r\n}\r\n\r\n.pulse {\r\n  -webkit-animation-name: pulse;\r\n  animation-name: pulse;\r\n}\r\n\r\n@-webkit-keyframes rubberBand {\r\n  from {\r\n    -webkit-transform: scale3d(1, 1, 1);\r\n    transform: scale3d(1, 1, 1);\r\n  }\r\n\r\n  30% {\r\n    -webkit-transform: scale3d(1.25, 0.75, 1);\r\n    transform: scale3d(1.25, 0.75, 1);\r\n  }\r\n\r\n  40% {\r\n    -webkit-transform: scale3d(0.75, 1.25, 1);\r\n    transform: scale3d(0.75, 1.25, 1);\r\n  }\r\n\r\n  50% {\r\n    -webkit-transform: scale3d(1.15, 0.85, 1);\r\n    transform: scale3d(1.15, 0.85, 1);\r\n  }\r\n\r\n  65% {\r\n    -webkit-transform: scale3d(0.95, 1.05, 1);\r\n    transform: scale3d(0.95, 1.05, 1);\r\n  }\r\n\r\n  75% {\r\n    -webkit-transform: scale3d(1.05, 0.95, 1);\r\n    transform: scale3d(1.05, 0.95, 1);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: scale3d(1, 1, 1);\r\n    transform: scale3d(1, 1, 1);\r\n  }\r\n}\r\n\r\n@keyframes rubberBand {\r\n  from {\r\n    -webkit-transform: scale3d(1, 1, 1);\r\n    transform: scale3d(1, 1, 1);\r\n  }\r\n\r\n  30% {\r\n    -webkit-transform: scale3d(1.25, 0.75, 1);\r\n    transform: scale3d(1.25, 0.75, 1);\r\n  }\r\n\r\n  40% {\r\n    -webkit-transform: scale3d(0.75, 1.25, 1);\r\n    transform: scale3d(0.75, 1.25, 1);\r\n  }\r\n\r\n  50% {\r\n    -webkit-transform: scale3d(1.15, 0.85, 1);\r\n    transform: scale3d(1.15, 0.85, 1);\r\n  }\r\n\r\n  65% {\r\n    -webkit-transform: scale3d(0.95, 1.05, 1);\r\n    transform: scale3d(0.95, 1.05, 1);\r\n  }\r\n\r\n  75% {\r\n    -webkit-transform: scale3d(1.05, 0.95, 1);\r\n    transform: scale3d(1.05, 0.95, 1);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: scale3d(1, 1, 1);\r\n    transform: scale3d(1, 1, 1);\r\n  }\r\n}\r\n\r\n.rubberBand {\r\n  -webkit-animation-name: rubberBand;\r\n  animation-name: rubberBand;\r\n}\r\n\r\n@-webkit-keyframes shake {\r\n  from,\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n\r\n  10%,\r\n  30%,\r\n  50%,\r\n  70%,\r\n  90% {\r\n    -webkit-transform: translate3d(-10px, 0, 0);\r\n    transform: translate3d(-10px, 0, 0);\r\n  }\r\n\r\n  20%,\r\n  40%,\r\n  60%,\r\n  80% {\r\n    -webkit-transform: translate3d(10px, 0, 0);\r\n    transform: translate3d(10px, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes shake {\r\n  from,\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n\r\n  10%,\r\n  30%,\r\n  50%,\r\n  70%,\r\n  90% {\r\n    -webkit-transform: translate3d(-10px, 0, 0);\r\n    transform: translate3d(-10px, 0, 0);\r\n  }\r\n\r\n  20%,\r\n  40%,\r\n  60%,\r\n  80% {\r\n    -webkit-transform: translate3d(10px, 0, 0);\r\n    transform: translate3d(10px, 0, 0);\r\n  }\r\n}\r\n\r\n.shake {\r\n  -webkit-animation-name: shake;\r\n  animation-name: shake;\r\n}\r\n\r\n@-webkit-keyframes headShake {\r\n  0% {\r\n    -webkit-transform: translateX(0);\r\n    transform: translateX(0);\r\n  }\r\n\r\n  6.5% {\r\n    -webkit-transform: translateX(-6px) rotateY(-9deg);\r\n    transform: translateX(-6px) rotateY(-9deg);\r\n  }\r\n\r\n  18.5% {\r\n    -webkit-transform: translateX(5px) rotateY(7deg);\r\n    transform: translateX(5px) rotateY(7deg);\r\n  }\r\n\r\n  31.5% {\r\n    -webkit-transform: translateX(-3px) rotateY(-5deg);\r\n    transform: translateX(-3px) rotateY(-5deg);\r\n  }\r\n\r\n  43.5% {\r\n    -webkit-transform: translateX(2px) rotateY(3deg);\r\n    transform: translateX(2px) rotateY(3deg);\r\n  }\r\n\r\n  50% {\r\n    -webkit-transform: translateX(0);\r\n    transform: translateX(0);\r\n  }\r\n}\r\n\r\n@keyframes headShake {\r\n  0% {\r\n    -webkit-transform: translateX(0);\r\n    transform: translateX(0);\r\n  }\r\n\r\n  6.5% {\r\n    -webkit-transform: translateX(-6px) rotateY(-9deg);\r\n    transform: translateX(-6px) rotateY(-9deg);\r\n  }\r\n\r\n  18.5% {\r\n    -webkit-transform: translateX(5px) rotateY(7deg);\r\n    transform: translateX(5px) rotateY(7deg);\r\n  }\r\n\r\n  31.5% {\r\n    -webkit-transform: translateX(-3px) rotateY(-5deg);\r\n    transform: translateX(-3px) rotateY(-5deg);\r\n  }\r\n\r\n  43.5% {\r\n    -webkit-transform: translateX(2px) rotateY(3deg);\r\n    transform: translateX(2px) rotateY(3deg);\r\n  }\r\n\r\n  50% {\r\n    -webkit-transform: translateX(0);\r\n    transform: translateX(0);\r\n  }\r\n}\r\n\r\n.headShake {\r\n  -webkit-animation-timing-function: ease-in-out;\r\n  animation-timing-function: ease-in-out;\r\n  -webkit-animation-name: headShake;\r\n  animation-name: headShake;\r\n}\r\n\r\n@-webkit-keyframes swing {\r\n  20% {\r\n    -webkit-transform: rotate3d(0, 0, 1, 15deg);\r\n    transform: rotate3d(0, 0, 1, 15deg);\r\n  }\r\n\r\n  40% {\r\n    -webkit-transform: rotate3d(0, 0, 1, -10deg);\r\n    transform: rotate3d(0, 0, 1, -10deg);\r\n  }\r\n\r\n  60% {\r\n    -webkit-transform: rotate3d(0, 0, 1, 5deg);\r\n    transform: rotate3d(0, 0, 1, 5deg);\r\n  }\r\n\r\n  80% {\r\n    -webkit-transform: rotate3d(0, 0, 1, -5deg);\r\n    transform: rotate3d(0, 0, 1, -5deg);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: rotate3d(0, 0, 1, 0deg);\r\n    transform: rotate3d(0, 0, 1, 0deg);\r\n  }\r\n}\r\n\r\n@keyframes swing {\r\n  20% {\r\n    -webkit-transform: rotate3d(0, 0, 1, 15deg);\r\n    transform: rotate3d(0, 0, 1, 15deg);\r\n  }\r\n\r\n  40% {\r\n    -webkit-transform: rotate3d(0, 0, 1, -10deg);\r\n    transform: rotate3d(0, 0, 1, -10deg);\r\n  }\r\n\r\n  60% {\r\n    -webkit-transform: rotate3d(0, 0, 1, 5deg);\r\n    transform: rotate3d(0, 0, 1, 5deg);\r\n  }\r\n\r\n  80% {\r\n    -webkit-transform: rotate3d(0, 0, 1, -5deg);\r\n    transform: rotate3d(0, 0, 1, -5deg);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: rotate3d(0, 0, 1, 0deg);\r\n    transform: rotate3d(0, 0, 1, 0deg);\r\n  }\r\n}\r\n\r\n.swing {\r\n  -webkit-transform-origin: top center;\r\n  transform-origin: top center;\r\n  -webkit-animation-name: swing;\r\n  animation-name: swing;\r\n}\r\n\r\n@-webkit-keyframes tada {\r\n  from {\r\n    -webkit-transform: scale3d(1, 1, 1);\r\n    transform: scale3d(1, 1, 1);\r\n  }\r\n\r\n  10%,\r\n  20% {\r\n    -webkit-transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg);\r\n    transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg);\r\n  }\r\n\r\n  30%,\r\n  50%,\r\n  70%,\r\n  90% {\r\n    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);\r\n    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);\r\n  }\r\n\r\n  40%,\r\n  60%,\r\n  80% {\r\n    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);\r\n    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: scale3d(1, 1, 1);\r\n    transform: scale3d(1, 1, 1);\r\n  }\r\n}\r\n\r\n@keyframes tada {\r\n  from {\r\n    -webkit-transform: scale3d(1, 1, 1);\r\n    transform: scale3d(1, 1, 1);\r\n  }\r\n\r\n  10%,\r\n  20% {\r\n    -webkit-transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg);\r\n    transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg);\r\n  }\r\n\r\n  30%,\r\n  50%,\r\n  70%,\r\n  90% {\r\n    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);\r\n    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);\r\n  }\r\n\r\n  40%,\r\n  60%,\r\n  80% {\r\n    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);\r\n    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: scale3d(1, 1, 1);\r\n    transform: scale3d(1, 1, 1);\r\n  }\r\n}\r\n\r\n.tada {\r\n  -webkit-animation-name: tada;\r\n  animation-name: tada;\r\n}\r\n\r\n/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */\r\n\r\n@-webkit-keyframes wobble {\r\n  from {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n\r\n  15% {\r\n    -webkit-transform: translate3d(-25%, 0, 0) rotate3d(0, 0, 1, -5deg);\r\n    transform: translate3d(-25%, 0, 0) rotate3d(0, 0, 1, -5deg);\r\n  }\r\n\r\n  30% {\r\n    -webkit-transform: translate3d(20%, 0, 0) rotate3d(0, 0, 1, 3deg);\r\n    transform: translate3d(20%, 0, 0) rotate3d(0, 0, 1, 3deg);\r\n  }\r\n\r\n  45% {\r\n    -webkit-transform: translate3d(-15%, 0, 0) rotate3d(0, 0, 1, -3deg);\r\n    transform: translate3d(-15%, 0, 0) rotate3d(0, 0, 1, -3deg);\r\n  }\r\n\r\n  60% {\r\n    -webkit-transform: translate3d(10%, 0, 0) rotate3d(0, 0, 1, 2deg);\r\n    transform: translate3d(10%, 0, 0) rotate3d(0, 0, 1, 2deg);\r\n  }\r\n\r\n  75% {\r\n    -webkit-transform: translate3d(-5%, 0, 0) rotate3d(0, 0, 1, -1deg);\r\n    transform: translate3d(-5%, 0, 0) rotate3d(0, 0, 1, -1deg);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes wobble {\r\n  from {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n\r\n  15% {\r\n    -webkit-transform: translate3d(-25%, 0, 0) rotate3d(0, 0, 1, -5deg);\r\n    transform: translate3d(-25%, 0, 0) rotate3d(0, 0, 1, -5deg);\r\n  }\r\n\r\n  30% {\r\n    -webkit-transform: translate3d(20%, 0, 0) rotate3d(0, 0, 1, 3deg);\r\n    transform: translate3d(20%, 0, 0) rotate3d(0, 0, 1, 3deg);\r\n  }\r\n\r\n  45% {\r\n    -webkit-transform: translate3d(-15%, 0, 0) rotate3d(0, 0, 1, -3deg);\r\n    transform: translate3d(-15%, 0, 0) rotate3d(0, 0, 1, -3deg);\r\n  }\r\n\r\n  60% {\r\n    -webkit-transform: translate3d(10%, 0, 0) rotate3d(0, 0, 1, 2deg);\r\n    transform: translate3d(10%, 0, 0) rotate3d(0, 0, 1, 2deg);\r\n  }\r\n\r\n  75% {\r\n    -webkit-transform: translate3d(-5%, 0, 0) rotate3d(0, 0, 1, -1deg);\r\n    transform: translate3d(-5%, 0, 0) rotate3d(0, 0, 1, -1deg);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.wobble {\r\n  -webkit-animation-name: wobble;\r\n  animation-name: wobble;\r\n}\r\n\r\n@-webkit-keyframes jello {\r\n  from,\r\n  11.1%,\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n\r\n  22.2% {\r\n    -webkit-transform: skewX(-12.5deg) skewY(-12.5deg);\r\n    transform: skewX(-12.5deg) skewY(-12.5deg);\r\n  }\r\n\r\n  33.3% {\r\n    -webkit-transform: skewX(6.25deg) skewY(6.25deg);\r\n    transform: skewX(6.25deg) skewY(6.25deg);\r\n  }\r\n\r\n  44.4% {\r\n    -webkit-transform: skewX(-3.125deg) skewY(-3.125deg);\r\n    transform: skewX(-3.125deg) skewY(-3.125deg);\r\n  }\r\n\r\n  55.5% {\r\n    -webkit-transform: skewX(1.5625deg) skewY(1.5625deg);\r\n    transform: skewX(1.5625deg) skewY(1.5625deg);\r\n  }\r\n\r\n  66.6% {\r\n    -webkit-transform: skewX(-0.78125deg) skewY(-0.78125deg);\r\n    transform: skewX(-0.78125deg) skewY(-0.78125deg);\r\n  }\r\n\r\n  77.7% {\r\n    -webkit-transform: skewX(0.390625deg) skewY(0.390625deg);\r\n    transform: skewX(0.390625deg) skewY(0.390625deg);\r\n  }\r\n\r\n  88.8% {\r\n    -webkit-transform: skewX(-0.1953125deg) skewY(-0.1953125deg);\r\n    transform: skewX(-0.1953125deg) skewY(-0.1953125deg);\r\n  }\r\n}\r\n\r\n@keyframes jello {\r\n  from,\r\n  11.1%,\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n\r\n  22.2% {\r\n    -webkit-transform: skewX(-12.5deg) skewY(-12.5deg);\r\n    transform: skewX(-12.5deg) skewY(-12.5deg);\r\n  }\r\n\r\n  33.3% {\r\n    -webkit-transform: skewX(6.25deg) skewY(6.25deg);\r\n    transform: skewX(6.25deg) skewY(6.25deg);\r\n  }\r\n\r\n  44.4% {\r\n    -webkit-transform: skewX(-3.125deg) skewY(-3.125deg);\r\n    transform: skewX(-3.125deg) skewY(-3.125deg);\r\n  }\r\n\r\n  55.5% {\r\n    -webkit-transform: skewX(1.5625deg) skewY(1.5625deg);\r\n    transform: skewX(1.5625deg) skewY(1.5625deg);\r\n  }\r\n\r\n  66.6% {\r\n    -webkit-transform: skewX(-0.78125deg) skewY(-0.78125deg);\r\n    transform: skewX(-0.78125deg) skewY(-0.78125deg);\r\n  }\r\n\r\n  77.7% {\r\n    -webkit-transform: skewX(0.390625deg) skewY(0.390625deg);\r\n    transform: skewX(0.390625deg) skewY(0.390625deg);\r\n  }\r\n\r\n  88.8% {\r\n    -webkit-transform: skewX(-0.1953125deg) skewY(-0.1953125deg);\r\n    transform: skewX(-0.1953125deg) skewY(-0.1953125deg);\r\n  }\r\n}\r\n\r\n.jello {\r\n  -webkit-animation-name: jello;\r\n  animation-name: jello;\r\n  -webkit-transform-origin: center;\r\n  transform-origin: center;\r\n}\r\n\r\n@-webkit-keyframes heartBeat {\r\n  0% {\r\n    -webkit-transform: scale(1);\r\n    transform: scale(1);\r\n  }\r\n\r\n  14% {\r\n    -webkit-transform: scale(1.3);\r\n    transform: scale(1.3);\r\n  }\r\n\r\n  28% {\r\n    -webkit-transform: scale(1);\r\n    transform: scale(1);\r\n  }\r\n\r\n  42% {\r\n    -webkit-transform: scale(1.3);\r\n    transform: scale(1.3);\r\n  }\r\n\r\n  70% {\r\n    -webkit-transform: scale(1);\r\n    transform: scale(1);\r\n  }\r\n}\r\n\r\n@keyframes heartBeat {\r\n  0% {\r\n    -webkit-transform: scale(1);\r\n    transform: scale(1);\r\n  }\r\n\r\n  14% {\r\n    -webkit-transform: scale(1.3);\r\n    transform: scale(1.3);\r\n  }\r\n\r\n  28% {\r\n    -webkit-transform: scale(1);\r\n    transform: scale(1);\r\n  }\r\n\r\n  42% {\r\n    -webkit-transform: scale(1.3);\r\n    transform: scale(1.3);\r\n  }\r\n\r\n  70% {\r\n    -webkit-transform: scale(1);\r\n    transform: scale(1);\r\n  }\r\n}\r\n\r\n.heartBeat {\r\n  -webkit-animation-name: heartBeat;\r\n  animation-name: heartBeat;\r\n  -webkit-animation-duration: 1.3s;\r\n  animation-duration: 1.3s;\r\n  -webkit-animation-timing-function: ease-in-out;\r\n  animation-timing-function: ease-in-out;\r\n}\r\n\r\n@-webkit-keyframes bounceIn {\r\n  from,\r\n  20%,\r\n  40%,\r\n  60%,\r\n  80%,\r\n  to {\r\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n  }\r\n\r\n  0% {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\r\n    transform: scale3d(0.3, 0.3, 0.3);\r\n  }\r\n\r\n  20% {\r\n    -webkit-transform: scale3d(1.1, 1.1, 1.1);\r\n    transform: scale3d(1.1, 1.1, 1.1);\r\n  }\r\n\r\n  40% {\r\n    -webkit-transform: scale3d(0.9, 0.9, 0.9);\r\n    transform: scale3d(0.9, 0.9, 0.9);\r\n  }\r\n\r\n  60% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(1.03, 1.03, 1.03);\r\n    transform: scale3d(1.03, 1.03, 1.03);\r\n  }\r\n\r\n  80% {\r\n    -webkit-transform: scale3d(0.97, 0.97, 0.97);\r\n    transform: scale3d(0.97, 0.97, 0.97);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(1, 1, 1);\r\n    transform: scale3d(1, 1, 1);\r\n  }\r\n}\r\n\r\n@keyframes bounceIn {\r\n  from,\r\n  20%,\r\n  40%,\r\n  60%,\r\n  80%,\r\n  to {\r\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n  }\r\n\r\n  0% {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\r\n    transform: scale3d(0.3, 0.3, 0.3);\r\n  }\r\n\r\n  20% {\r\n    -webkit-transform: scale3d(1.1, 1.1, 1.1);\r\n    transform: scale3d(1.1, 1.1, 1.1);\r\n  }\r\n\r\n  40% {\r\n    -webkit-transform: scale3d(0.9, 0.9, 0.9);\r\n    transform: scale3d(0.9, 0.9, 0.9);\r\n  }\r\n\r\n  60% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(1.03, 1.03, 1.03);\r\n    transform: scale3d(1.03, 1.03, 1.03);\r\n  }\r\n\r\n  80% {\r\n    -webkit-transform: scale3d(0.97, 0.97, 0.97);\r\n    transform: scale3d(0.97, 0.97, 0.97);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(1, 1, 1);\r\n    transform: scale3d(1, 1, 1);\r\n  }\r\n}\r\n\r\n.bounceIn {\r\n  -webkit-animation-duration: 0.75s;\r\n  animation-duration: 0.75s;\r\n  -webkit-animation-name: bounceIn;\r\n  animation-name: bounceIn;\r\n}\r\n\r\n@-webkit-keyframes bounceInDown {\r\n  from,\r\n  60%,\r\n  75%,\r\n  90%,\r\n  to {\r\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n  }\r\n\r\n  0% {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, -3000px, 0);\r\n    transform: translate3d(0, -3000px, 0);\r\n  }\r\n\r\n  60% {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 25px, 0);\r\n    transform: translate3d(0, 25px, 0);\r\n  }\r\n\r\n  75% {\r\n    -webkit-transform: translate3d(0, -10px, 0);\r\n    transform: translate3d(0, -10px, 0);\r\n  }\r\n\r\n  90% {\r\n    -webkit-transform: translate3d(0, 5px, 0);\r\n    transform: translate3d(0, 5px, 0);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes bounceInDown {\r\n  from,\r\n  60%,\r\n  75%,\r\n  90%,\r\n  to {\r\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n  }\r\n\r\n  0% {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, -3000px, 0);\r\n    transform: translate3d(0, -3000px, 0);\r\n  }\r\n\r\n  60% {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 25px, 0);\r\n    transform: translate3d(0, 25px, 0);\r\n  }\r\n\r\n  75% {\r\n    -webkit-transform: translate3d(0, -10px, 0);\r\n    transform: translate3d(0, -10px, 0);\r\n  }\r\n\r\n  90% {\r\n    -webkit-transform: translate3d(0, 5px, 0);\r\n    transform: translate3d(0, 5px, 0);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.bounceInDown {\r\n  -webkit-animation-name: bounceInDown;\r\n  animation-name: bounceInDown;\r\n}\r\n\r\n@-webkit-keyframes bounceInLeft {\r\n  from,\r\n  60%,\r\n  75%,\r\n  90%,\r\n  to {\r\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n  }\r\n\r\n  0% {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(-3000px, 0, 0);\r\n    transform: translate3d(-3000px, 0, 0);\r\n  }\r\n\r\n  60% {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(25px, 0, 0);\r\n    transform: translate3d(25px, 0, 0);\r\n  }\r\n\r\n  75% {\r\n    -webkit-transform: translate3d(-10px, 0, 0);\r\n    transform: translate3d(-10px, 0, 0);\r\n  }\r\n\r\n  90% {\r\n    -webkit-transform: translate3d(5px, 0, 0);\r\n    transform: translate3d(5px, 0, 0);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes bounceInLeft {\r\n  from,\r\n  60%,\r\n  75%,\r\n  90%,\r\n  to {\r\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n  }\r\n\r\n  0% {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(-3000px, 0, 0);\r\n    transform: translate3d(-3000px, 0, 0);\r\n  }\r\n\r\n  60% {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(25px, 0, 0);\r\n    transform: translate3d(25px, 0, 0);\r\n  }\r\n\r\n  75% {\r\n    -webkit-transform: translate3d(-10px, 0, 0);\r\n    transform: translate3d(-10px, 0, 0);\r\n  }\r\n\r\n  90% {\r\n    -webkit-transform: translate3d(5px, 0, 0);\r\n    transform: translate3d(5px, 0, 0);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.bounceInLeft {\r\n  -webkit-animation-name: bounceInLeft;\r\n  animation-name: bounceInLeft;\r\n}\r\n\r\n@-webkit-keyframes bounceInRight {\r\n  from,\r\n  60%,\r\n  75%,\r\n  90%,\r\n  to {\r\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n  }\r\n\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(3000px, 0, 0);\r\n    transform: translate3d(3000px, 0, 0);\r\n  }\r\n\r\n  60% {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(-25px, 0, 0);\r\n    transform: translate3d(-25px, 0, 0);\r\n  }\r\n\r\n  75% {\r\n    -webkit-transform: translate3d(10px, 0, 0);\r\n    transform: translate3d(10px, 0, 0);\r\n  }\r\n\r\n  90% {\r\n    -webkit-transform: translate3d(-5px, 0, 0);\r\n    transform: translate3d(-5px, 0, 0);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes bounceInRight {\r\n  from,\r\n  60%,\r\n  75%,\r\n  90%,\r\n  to {\r\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n  }\r\n\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(3000px, 0, 0);\r\n    transform: translate3d(3000px, 0, 0);\r\n  }\r\n\r\n  60% {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(-25px, 0, 0);\r\n    transform: translate3d(-25px, 0, 0);\r\n  }\r\n\r\n  75% {\r\n    -webkit-transform: translate3d(10px, 0, 0);\r\n    transform: translate3d(10px, 0, 0);\r\n  }\r\n\r\n  90% {\r\n    -webkit-transform: translate3d(-5px, 0, 0);\r\n    transform: translate3d(-5px, 0, 0);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.bounceInRight {\r\n  -webkit-animation-name: bounceInRight;\r\n  animation-name: bounceInRight;\r\n}\r\n\r\n@-webkit-keyframes bounceInUp {\r\n  from,\r\n  60%,\r\n  75%,\r\n  90%,\r\n  to {\r\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n  }\r\n\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, 3000px, 0);\r\n    transform: translate3d(0, 3000px, 0);\r\n  }\r\n\r\n  60% {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, -20px, 0);\r\n    transform: translate3d(0, -20px, 0);\r\n  }\r\n\r\n  75% {\r\n    -webkit-transform: translate3d(0, 10px, 0);\r\n    transform: translate3d(0, 10px, 0);\r\n  }\r\n\r\n  90% {\r\n    -webkit-transform: translate3d(0, -5px, 0);\r\n    transform: translate3d(0, -5px, 0);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes bounceInUp {\r\n  from,\r\n  60%,\r\n  75%,\r\n  90%,\r\n  to {\r\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\r\n  }\r\n\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, 3000px, 0);\r\n    transform: translate3d(0, 3000px, 0);\r\n  }\r\n\r\n  60% {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, -20px, 0);\r\n    transform: translate3d(0, -20px, 0);\r\n  }\r\n\r\n  75% {\r\n    -webkit-transform: translate3d(0, 10px, 0);\r\n    transform: translate3d(0, 10px, 0);\r\n  }\r\n\r\n  90% {\r\n    -webkit-transform: translate3d(0, -5px, 0);\r\n    transform: translate3d(0, -5px, 0);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.bounceInUp {\r\n  -webkit-animation-name: bounceInUp;\r\n  animation-name: bounceInUp;\r\n}\r\n\r\n@-webkit-keyframes bounceOut {\r\n  20% {\r\n    -webkit-transform: scale3d(0.9, 0.9, 0.9);\r\n    transform: scale3d(0.9, 0.9, 0.9);\r\n  }\r\n\r\n  50%,\r\n  55% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(1.1, 1.1, 1.1);\r\n    transform: scale3d(1.1, 1.1, 1.1);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\r\n    transform: scale3d(0.3, 0.3, 0.3);\r\n  }\r\n}\r\n\r\n@keyframes bounceOut {\r\n  20% {\r\n    -webkit-transform: scale3d(0.9, 0.9, 0.9);\r\n    transform: scale3d(0.9, 0.9, 0.9);\r\n  }\r\n\r\n  50%,\r\n  55% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(1.1, 1.1, 1.1);\r\n    transform: scale3d(1.1, 1.1, 1.1);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\r\n    transform: scale3d(0.3, 0.3, 0.3);\r\n  }\r\n}\r\n\r\n.bounceOut {\r\n  -webkit-animation-duration: 0.75s;\r\n  animation-duration: 0.75s;\r\n  -webkit-animation-name: bounceOut;\r\n  animation-name: bounceOut;\r\n}\r\n\r\n@-webkit-keyframes bounceOutDown {\r\n  20% {\r\n    -webkit-transform: translate3d(0, 10px, 0);\r\n    transform: translate3d(0, 10px, 0);\r\n  }\r\n\r\n  40%,\r\n  45% {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, -20px, 0);\r\n    transform: translate3d(0, -20px, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, 2000px, 0);\r\n    transform: translate3d(0, 2000px, 0);\r\n  }\r\n}\r\n\r\n@keyframes bounceOutDown {\r\n  20% {\r\n    -webkit-transform: translate3d(0, 10px, 0);\r\n    transform: translate3d(0, 10px, 0);\r\n  }\r\n\r\n  40%,\r\n  45% {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, -20px, 0);\r\n    transform: translate3d(0, -20px, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, 2000px, 0);\r\n    transform: translate3d(0, 2000px, 0);\r\n  }\r\n}\r\n\r\n.bounceOutDown {\r\n  -webkit-animation-name: bounceOutDown;\r\n  animation-name: bounceOutDown;\r\n}\r\n\r\n@-webkit-keyframes bounceOutLeft {\r\n  20% {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(20px, 0, 0);\r\n    transform: translate3d(20px, 0, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(-2000px, 0, 0);\r\n    transform: translate3d(-2000px, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes bounceOutLeft {\r\n  20% {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(20px, 0, 0);\r\n    transform: translate3d(20px, 0, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(-2000px, 0, 0);\r\n    transform: translate3d(-2000px, 0, 0);\r\n  }\r\n}\r\n\r\n.bounceOutLeft {\r\n  -webkit-animation-name: bounceOutLeft;\r\n  animation-name: bounceOutLeft;\r\n}\r\n\r\n@-webkit-keyframes bounceOutRight {\r\n  20% {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(-20px, 0, 0);\r\n    transform: translate3d(-20px, 0, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(2000px, 0, 0);\r\n    transform: translate3d(2000px, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes bounceOutRight {\r\n  20% {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(-20px, 0, 0);\r\n    transform: translate3d(-20px, 0, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(2000px, 0, 0);\r\n    transform: translate3d(2000px, 0, 0);\r\n  }\r\n}\r\n\r\n.bounceOutRight {\r\n  -webkit-animation-name: bounceOutRight;\r\n  animation-name: bounceOutRight;\r\n}\r\n\r\n@-webkit-keyframes bounceOutUp {\r\n  20% {\r\n    -webkit-transform: translate3d(0, -10px, 0);\r\n    transform: translate3d(0, -10px, 0);\r\n  }\r\n\r\n  40%,\r\n  45% {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 20px, 0);\r\n    transform: translate3d(0, 20px, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, -2000px, 0);\r\n    transform: translate3d(0, -2000px, 0);\r\n  }\r\n}\r\n\r\n@keyframes bounceOutUp {\r\n  20% {\r\n    -webkit-transform: translate3d(0, -10px, 0);\r\n    transform: translate3d(0, -10px, 0);\r\n  }\r\n\r\n  40%,\r\n  45% {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 20px, 0);\r\n    transform: translate3d(0, 20px, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, -2000px, 0);\r\n    transform: translate3d(0, -2000px, 0);\r\n  }\r\n}\r\n\r\n.bounceOutUp {\r\n  -webkit-animation-name: bounceOutUp;\r\n  animation-name: bounceOutUp;\r\n}\r\n\r\n@-webkit-keyframes fadeIn {\r\n  from {\r\n    opacity: 0;\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n  }\r\n}\r\n\r\n@keyframes fadeIn {\r\n  from {\r\n    opacity: 0;\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n  }\r\n}\r\n\r\n.fadeIn {\r\n  -webkit-animation-name: fadeIn;\r\n  animation-name: fadeIn;\r\n}\r\n\r\n@keyframes fadeInDown {\r\n  from {\r\n    opacity: 0;\r\n    transform: translate3d(0, -100%, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.fadeInDown {\r\n  animation-name: fadeInDown;\r\n}\r\n\r\n@-webkit-keyframes fadeInDownBig {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, -2000px, 0);\r\n    transform: translate3d(0, -2000px, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes fadeInDownBig {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, -2000px, 0);\r\n    transform: translate3d(0, -2000px, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.fadeInDownBig {\r\n  -webkit-animation-name: fadeInDownBig;\r\n  animation-name: fadeInDownBig;\r\n}\r\n\r\n@-webkit-keyframes fadeInLeft {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(-100%, 0, 0);\r\n    transform: translate3d(-100%, 0, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes fadeInLeft {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(-100%, 0, 0);\r\n    transform: translate3d(-100%, 0, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.fadeInLeft {\r\n  -webkit-animation-name: fadeInLeft;\r\n  animation-name: fadeInLeft;\r\n}\r\n\r\n@-webkit-keyframes fadeInLeftBig {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(-2000px, 0, 0);\r\n    transform: translate3d(-2000px, 0, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes fadeInLeftBig {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(-2000px, 0, 0);\r\n    transform: translate3d(-2000px, 0, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.fadeInLeftBig {\r\n  -webkit-animation-name: fadeInLeftBig;\r\n  animation-name: fadeInLeftBig;\r\n}\r\n\r\n@-webkit-keyframes fadeInRight {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(100%, 0, 0);\r\n    transform: translate3d(100%, 0, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes fadeInRight {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(100%, 0, 0);\r\n    transform: translate3d(100%, 0, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.fadeInRight {\r\n  -webkit-animation-name: fadeInRight;\r\n  animation-name: fadeInRight;\r\n}\r\n\r\n@-webkit-keyframes fadeInRightBig {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(2000px, 0, 0);\r\n    transform: translate3d(2000px, 0, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes fadeInRightBig {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(2000px, 0, 0);\r\n    transform: translate3d(2000px, 0, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.fadeInRightBig {\r\n  -webkit-animation-name: fadeInRightBig;\r\n  animation-name: fadeInRightBig;\r\n}\r\n\r\n@-webkit-keyframes fadeInUp {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, 100%, 0);\r\n    transform: translate3d(0, 100%, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes fadeInUp {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, 100%, 0);\r\n    transform: translate3d(0, 100%, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.fadeInUp {\r\n  -webkit-animation-name: fadeInUp;\r\n  animation-name: fadeInUp;\r\n}\r\n\r\n@-webkit-keyframes fadeInUpBig {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, 2000px, 0);\r\n    transform: translate3d(0, 2000px, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes fadeInUpBig {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, 2000px, 0);\r\n    transform: translate3d(0, 2000px, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.fadeInUpBig {\r\n  -webkit-animation-name: fadeInUpBig;\r\n  animation-name: fadeInUpBig;\r\n}\r\n\r\n@-webkit-keyframes fadeOut {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n@keyframes fadeOut {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n.fadeOut {\r\n  -webkit-animation-name: fadeOut;\r\n  animation-name: fadeOut;\r\n}\r\n\r\n@-webkit-keyframes fadeOutDown {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, 100%, 0);\r\n    transform: translate3d(0, 100%, 0);\r\n  }\r\n}\r\n\r\n@keyframes fadeOutDown {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, 100%, 0);\r\n    transform: translate3d(0, 100%, 0);\r\n  }\r\n}\r\n\r\n.fadeOutDown {\r\n  -webkit-animation-name: fadeOutDown;\r\n  animation-name: fadeOutDown;\r\n}\r\n\r\n@-webkit-keyframes fadeOutDownBig {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, 2000px, 0);\r\n    transform: translate3d(0, 2000px, 0);\r\n  }\r\n}\r\n\r\n@keyframes fadeOutDownBig {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, 2000px, 0);\r\n    transform: translate3d(0, 2000px, 0);\r\n  }\r\n}\r\n\r\n.fadeOutDownBig {\r\n  -webkit-animation-name: fadeOutDownBig;\r\n  animation-name: fadeOutDownBig;\r\n}\r\n\r\n@-webkit-keyframes fadeOutLeft {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(-100%, 0, 0);\r\n    transform: translate3d(-100%, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes fadeOutLeft {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(-100%, 0, 0);\r\n    transform: translate3d(-100%, 0, 0);\r\n  }\r\n}\r\n\r\n.fadeOutLeft {\r\n  -webkit-animation-name: fadeOutLeft;\r\n  animation-name: fadeOutLeft;\r\n}\r\n\r\n@-webkit-keyframes fadeOutLeftBig {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(-2000px, 0, 0);\r\n    transform: translate3d(-2000px, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes fadeOutLeftBig {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(-2000px, 0, 0);\r\n    transform: translate3d(-2000px, 0, 0);\r\n  }\r\n}\r\n\r\n.fadeOutLeftBig {\r\n  -webkit-animation-name: fadeOutLeftBig;\r\n  animation-name: fadeOutLeftBig;\r\n}\r\n\r\n@-webkit-keyframes fadeOutRight {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(100%, 0, 0);\r\n    transform: translate3d(100%, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes fadeOutRight {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(100%, 0, 0);\r\n    transform: translate3d(100%, 0, 0);\r\n  }\r\n}\r\n\r\n.fadeOutRight {\r\n  -webkit-animation-name: fadeOutRight;\r\n  animation-name: fadeOutRight;\r\n}\r\n\r\n@-webkit-keyframes fadeOutRightBig {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(2000px, 0, 0);\r\n    transform: translate3d(2000px, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes fadeOutRightBig {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(2000px, 0, 0);\r\n    transform: translate3d(2000px, 0, 0);\r\n  }\r\n}\r\n\r\n.fadeOutRightBig {\r\n  -webkit-animation-name: fadeOutRightBig;\r\n  animation-name: fadeOutRightBig;\r\n}\r\n\r\n@-webkit-keyframes fadeOutUp {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, -100%, 0);\r\n    transform: translate3d(0, -100%, 0);\r\n  }\r\n}\r\n\r\n@keyframes fadeOutUp {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, -100%, 0);\r\n    transform: translate3d(0, -100%, 0);\r\n  }\r\n}\r\n\r\n.fadeOutUp {\r\n  -webkit-animation-name: fadeOutUp;\r\n  animation-name: fadeOutUp;\r\n}\r\n\r\n@-webkit-keyframes fadeOutUpBig {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, -2000px, 0);\r\n    transform: translate3d(0, -2000px, 0);\r\n  }\r\n}\r\n\r\n@keyframes fadeOutUpBig {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(0, -2000px, 0);\r\n    transform: translate3d(0, -2000px, 0);\r\n  }\r\n}\r\n\r\n.fadeOutUpBig {\r\n  -webkit-animation-name: fadeOutUpBig;\r\n  animation-name: fadeOutUpBig;\r\n}\r\n\r\n@-webkit-keyframes flip {\r\n  from {\r\n    -webkit-transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 0)\r\n      rotate3d(0, 1, 0, -360deg);\r\n    transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 0) rotate3d(0, 1, 0, -360deg);\r\n    -webkit-animation-timing-function: ease-out;\r\n    animation-timing-function: ease-out;\r\n  }\r\n\r\n  40% {\r\n    -webkit-transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 150px)\r\n      rotate3d(0, 1, 0, -190deg);\r\n    transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 150px)\r\n      rotate3d(0, 1, 0, -190deg);\r\n    -webkit-animation-timing-function: ease-out;\r\n    animation-timing-function: ease-out;\r\n  }\r\n\r\n  50% {\r\n    -webkit-transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 150px)\r\n      rotate3d(0, 1, 0, -170deg);\r\n    transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 150px)\r\n      rotate3d(0, 1, 0, -170deg);\r\n    -webkit-animation-timing-function: ease-in;\r\n    animation-timing-function: ease-in;\r\n  }\r\n\r\n  80% {\r\n    -webkit-transform: perspective(400px) scale3d(0.95, 0.95, 0.95) translate3d(0, 0, 0)\r\n      rotate3d(0, 1, 0, 0deg);\r\n    transform: perspective(400px) scale3d(0.95, 0.95, 0.95) translate3d(0, 0, 0)\r\n      rotate3d(0, 1, 0, 0deg);\r\n    -webkit-animation-timing-function: ease-in;\r\n    animation-timing-function: ease-in;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 0)\r\n      rotate3d(0, 1, 0, 0deg);\r\n    transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 0) rotate3d(0, 1, 0, 0deg);\r\n    -webkit-animation-timing-function: ease-in;\r\n    animation-timing-function: ease-in;\r\n  }\r\n}\r\n\r\n@keyframes flip {\r\n  from {\r\n    -webkit-transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 0)\r\n      rotate3d(0, 1, 0, -360deg);\r\n    transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 0) rotate3d(0, 1, 0, -360deg);\r\n    -webkit-animation-timing-function: ease-out;\r\n    animation-timing-function: ease-out;\r\n  }\r\n\r\n  40% {\r\n    -webkit-transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 150px)\r\n      rotate3d(0, 1, 0, -190deg);\r\n    transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 150px)\r\n      rotate3d(0, 1, 0, -190deg);\r\n    -webkit-animation-timing-function: ease-out;\r\n    animation-timing-function: ease-out;\r\n  }\r\n\r\n  50% {\r\n    -webkit-transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 150px)\r\n      rotate3d(0, 1, 0, -170deg);\r\n    transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 150px)\r\n      rotate3d(0, 1, 0, -170deg);\r\n    -webkit-animation-timing-function: ease-in;\r\n    animation-timing-function: ease-in;\r\n  }\r\n\r\n  80% {\r\n    -webkit-transform: perspective(400px) scale3d(0.95, 0.95, 0.95) translate3d(0, 0, 0)\r\n      rotate3d(0, 1, 0, 0deg);\r\n    transform: perspective(400px) scale3d(0.95, 0.95, 0.95) translate3d(0, 0, 0)\r\n      rotate3d(0, 1, 0, 0deg);\r\n    -webkit-animation-timing-function: ease-in;\r\n    animation-timing-function: ease-in;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 0)\r\n      rotate3d(0, 1, 0, 0deg);\r\n    transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 0) rotate3d(0, 1, 0, 0deg);\r\n    -webkit-animation-timing-function: ease-in;\r\n    animation-timing-function: ease-in;\r\n  }\r\n}\r\n\r\n.animated.flip {\r\n  -webkit-backface-visibility: visible;\r\n  backface-visibility: visible;\r\n  -webkit-animation-name: flip;\r\n  animation-name: flip;\r\n}\r\n\r\n@-webkit-keyframes flipInX {\r\n  from {\r\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\r\n    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\r\n    -webkit-animation-timing-function: ease-in;\r\n    animation-timing-function: ease-in;\r\n    opacity: 0;\r\n  }\r\n\r\n  40% {\r\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\r\n    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\r\n    -webkit-animation-timing-function: ease-in;\r\n    animation-timing-function: ease-in;\r\n  }\r\n\r\n  60% {\r\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 10deg);\r\n    transform: perspective(400px) rotate3d(1, 0, 0, 10deg);\r\n    opacity: 1;\r\n  }\r\n\r\n  80% {\r\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -5deg);\r\n    transform: perspective(400px) rotate3d(1, 0, 0, -5deg);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: perspective(400px);\r\n    transform: perspective(400px);\r\n  }\r\n}\r\n\r\n@keyframes flipInX {\r\n  from {\r\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\r\n    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\r\n    -webkit-animation-timing-function: ease-in;\r\n    animation-timing-function: ease-in;\r\n    opacity: 0;\r\n  }\r\n\r\n  40% {\r\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\r\n    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\r\n    -webkit-animation-timing-function: ease-in;\r\n    animation-timing-function: ease-in;\r\n  }\r\n\r\n  60% {\r\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 10deg);\r\n    transform: perspective(400px) rotate3d(1, 0, 0, 10deg);\r\n    opacity: 1;\r\n  }\r\n\r\n  80% {\r\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -5deg);\r\n    transform: perspective(400px) rotate3d(1, 0, 0, -5deg);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: perspective(400px);\r\n    transform: perspective(400px);\r\n  }\r\n}\r\n\r\n.flipInX {\r\n  -webkit-backface-visibility: visible !important;\r\n  backface-visibility: visible !important;\r\n  -webkit-animation-name: flipInX;\r\n  animation-name: flipInX;\r\n}\r\n\r\n@-webkit-keyframes flipInY {\r\n  from {\r\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\r\n    transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\r\n    -webkit-animation-timing-function: ease-in;\r\n    animation-timing-function: ease-in;\r\n    opacity: 0;\r\n  }\r\n\r\n  40% {\r\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -20deg);\r\n    transform: perspective(400px) rotate3d(0, 1, 0, -20deg);\r\n    -webkit-animation-timing-function: ease-in;\r\n    animation-timing-function: ease-in;\r\n  }\r\n\r\n  60% {\r\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 10deg);\r\n    transform: perspective(400px) rotate3d(0, 1, 0, 10deg);\r\n    opacity: 1;\r\n  }\r\n\r\n  80% {\r\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -5deg);\r\n    transform: perspective(400px) rotate3d(0, 1, 0, -5deg);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: perspective(400px);\r\n    transform: perspective(400px);\r\n  }\r\n}\r\n\r\n@keyframes flipInY {\r\n  from {\r\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\r\n    transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\r\n    -webkit-animation-timing-function: ease-in;\r\n    animation-timing-function: ease-in;\r\n    opacity: 0;\r\n  }\r\n\r\n  40% {\r\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -20deg);\r\n    transform: perspective(400px) rotate3d(0, 1, 0, -20deg);\r\n    -webkit-animation-timing-function: ease-in;\r\n    animation-timing-function: ease-in;\r\n  }\r\n\r\n  60% {\r\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 10deg);\r\n    transform: perspective(400px) rotate3d(0, 1, 0, 10deg);\r\n    opacity: 1;\r\n  }\r\n\r\n  80% {\r\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -5deg);\r\n    transform: perspective(400px) rotate3d(0, 1, 0, -5deg);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: perspective(400px);\r\n    transform: perspective(400px);\r\n  }\r\n}\r\n\r\n.flipInY {\r\n  -webkit-backface-visibility: visible !important;\r\n  backface-visibility: visible !important;\r\n  -webkit-animation-name: flipInY;\r\n  animation-name: flipInY;\r\n}\r\n\r\n@-webkit-keyframes flipOutX {\r\n  from {\r\n    -webkit-transform: perspective(400px);\r\n    transform: perspective(400px);\r\n  }\r\n\r\n  30% {\r\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\r\n    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\r\n    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n@keyframes flipOutX {\r\n  from {\r\n    -webkit-transform: perspective(400px);\r\n    transform: perspective(400px);\r\n  }\r\n\r\n  30% {\r\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\r\n    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\r\n    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n.flipOutX {\r\n  -webkit-animation-duration: 0.75s;\r\n  animation-duration: 0.75s;\r\n  -webkit-animation-name: flipOutX;\r\n  animation-name: flipOutX;\r\n  -webkit-backface-visibility: visible !important;\r\n  backface-visibility: visible !important;\r\n}\r\n\r\n@-webkit-keyframes flipOutY {\r\n  from {\r\n    -webkit-transform: perspective(400px);\r\n    transform: perspective(400px);\r\n  }\r\n\r\n  30% {\r\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -15deg);\r\n    transform: perspective(400px) rotate3d(0, 1, 0, -15deg);\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\r\n    transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n@keyframes flipOutY {\r\n  from {\r\n    -webkit-transform: perspective(400px);\r\n    transform: perspective(400px);\r\n  }\r\n\r\n  30% {\r\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -15deg);\r\n    transform: perspective(400px) rotate3d(0, 1, 0, -15deg);\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\r\n    transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n.flipOutY {\r\n  -webkit-animation-duration: 0.75s;\r\n  animation-duration: 0.75s;\r\n  -webkit-backface-visibility: visible !important;\r\n  backface-visibility: visible !important;\r\n  -webkit-animation-name: flipOutY;\r\n  animation-name: flipOutY;\r\n}\r\n\r\n@-webkit-keyframes lightSpeedIn {\r\n  from {\r\n    -webkit-transform: translate3d(100%, 0, 0) skewX(-30deg);\r\n    transform: translate3d(100%, 0, 0) skewX(-30deg);\r\n    opacity: 0;\r\n  }\r\n\r\n  60% {\r\n    -webkit-transform: skewX(20deg);\r\n    transform: skewX(20deg);\r\n    opacity: 1;\r\n  }\r\n\r\n  80% {\r\n    -webkit-transform: skewX(-5deg);\r\n    transform: skewX(-5deg);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes lightSpeedIn {\r\n  from {\r\n    -webkit-transform: translate3d(100%, 0, 0) skewX(-30deg);\r\n    transform: translate3d(100%, 0, 0) skewX(-30deg);\r\n    opacity: 0;\r\n  }\r\n\r\n  60% {\r\n    -webkit-transform: skewX(20deg);\r\n    transform: skewX(20deg);\r\n    opacity: 1;\r\n  }\r\n\r\n  80% {\r\n    -webkit-transform: skewX(-5deg);\r\n    transform: skewX(-5deg);\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.lightSpeedIn {\r\n  -webkit-animation-name: lightSpeedIn;\r\n  animation-name: lightSpeedIn;\r\n  -webkit-animation-timing-function: ease-out;\r\n  animation-timing-function: ease-out;\r\n}\r\n\r\n@-webkit-keyframes lightSpeedOut {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(100%, 0, 0) skewX(30deg);\r\n    transform: translate3d(100%, 0, 0) skewX(30deg);\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n@keyframes lightSpeedOut {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(100%, 0, 0) skewX(30deg);\r\n    transform: translate3d(100%, 0, 0) skewX(30deg);\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n.lightSpeedOut {\r\n  -webkit-animation-name: lightSpeedOut;\r\n  animation-name: lightSpeedOut;\r\n  -webkit-animation-timing-function: ease-in;\r\n  animation-timing-function: ease-in;\r\n}\r\n\r\n@-webkit-keyframes rotateIn {\r\n  from {\r\n    -webkit-transform-origin: center;\r\n    transform-origin: center;\r\n    -webkit-transform: rotate3d(0, 0, 1, -200deg);\r\n    transform: rotate3d(0, 0, 1, -200deg);\r\n    opacity: 0;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: center;\r\n    transform-origin: center;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n    opacity: 1;\r\n  }\r\n}\r\n\r\n@keyframes rotateIn {\r\n  from {\r\n    -webkit-transform-origin: center;\r\n    transform-origin: center;\r\n    -webkit-transform: rotate3d(0, 0, 1, -200deg);\r\n    transform: rotate3d(0, 0, 1, -200deg);\r\n    opacity: 0;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: center;\r\n    transform-origin: center;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n    opacity: 1;\r\n  }\r\n}\r\n\r\n.rotateIn {\r\n  -webkit-animation-name: rotateIn;\r\n  animation-name: rotateIn;\r\n}\r\n\r\n@-webkit-keyframes rotateInDownLeft {\r\n  from {\r\n    -webkit-transform-origin: left bottom;\r\n    transform-origin: left bottom;\r\n    -webkit-transform: rotate3d(0, 0, 1, -45deg);\r\n    transform: rotate3d(0, 0, 1, -45deg);\r\n    opacity: 0;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: left bottom;\r\n    transform-origin: left bottom;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n    opacity: 1;\r\n  }\r\n}\r\n\r\n@keyframes rotateInDownLeft {\r\n  from {\r\n    -webkit-transform-origin: left bottom;\r\n    transform-origin: left bottom;\r\n    -webkit-transform: rotate3d(0, 0, 1, -45deg);\r\n    transform: rotate3d(0, 0, 1, -45deg);\r\n    opacity: 0;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: left bottom;\r\n    transform-origin: left bottom;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n    opacity: 1;\r\n  }\r\n}\r\n\r\n.rotateInDownLeft {\r\n  -webkit-animation-name: rotateInDownLeft;\r\n  animation-name: rotateInDownLeft;\r\n}\r\n\r\n@-webkit-keyframes rotateInDownRight {\r\n  from {\r\n    -webkit-transform-origin: right bottom;\r\n    transform-origin: right bottom;\r\n    -webkit-transform: rotate3d(0, 0, 1, 45deg);\r\n    transform: rotate3d(0, 0, 1, 45deg);\r\n    opacity: 0;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: right bottom;\r\n    transform-origin: right bottom;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n    opacity: 1;\r\n  }\r\n}\r\n\r\n@keyframes rotateInDownRight {\r\n  from {\r\n    -webkit-transform-origin: right bottom;\r\n    transform-origin: right bottom;\r\n    -webkit-transform: rotate3d(0, 0, 1, 45deg);\r\n    transform: rotate3d(0, 0, 1, 45deg);\r\n    opacity: 0;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: right bottom;\r\n    transform-origin: right bottom;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n    opacity: 1;\r\n  }\r\n}\r\n\r\n.rotateInDownRight {\r\n  -webkit-animation-name: rotateInDownRight;\r\n  animation-name: rotateInDownRight;\r\n}\r\n\r\n@-webkit-keyframes rotateInUpLeft {\r\n  from {\r\n    -webkit-transform-origin: left bottom;\r\n    transform-origin: left bottom;\r\n    -webkit-transform: rotate3d(0, 0, 1, 45deg);\r\n    transform: rotate3d(0, 0, 1, 45deg);\r\n    opacity: 0;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: left bottom;\r\n    transform-origin: left bottom;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n    opacity: 1;\r\n  }\r\n}\r\n\r\n@keyframes rotateInUpLeft {\r\n  from {\r\n    -webkit-transform-origin: left bottom;\r\n    transform-origin: left bottom;\r\n    -webkit-transform: rotate3d(0, 0, 1, 45deg);\r\n    transform: rotate3d(0, 0, 1, 45deg);\r\n    opacity: 0;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: left bottom;\r\n    transform-origin: left bottom;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n    opacity: 1;\r\n  }\r\n}\r\n\r\n.rotateInUpLeft {\r\n  -webkit-animation-name: rotateInUpLeft;\r\n  animation-name: rotateInUpLeft;\r\n}\r\n\r\n@-webkit-keyframes rotateInUpRight {\r\n  from {\r\n    -webkit-transform-origin: right bottom;\r\n    transform-origin: right bottom;\r\n    -webkit-transform: rotate3d(0, 0, 1, -90deg);\r\n    transform: rotate3d(0, 0, 1, -90deg);\r\n    opacity: 0;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: right bottom;\r\n    transform-origin: right bottom;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n    opacity: 1;\r\n  }\r\n}\r\n\r\n@keyframes rotateInUpRight {\r\n  from {\r\n    -webkit-transform-origin: right bottom;\r\n    transform-origin: right bottom;\r\n    -webkit-transform: rotate3d(0, 0, 1, -90deg);\r\n    transform: rotate3d(0, 0, 1, -90deg);\r\n    opacity: 0;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: right bottom;\r\n    transform-origin: right bottom;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n    opacity: 1;\r\n  }\r\n}\r\n\r\n.rotateInUpRight {\r\n  -webkit-animation-name: rotateInUpRight;\r\n  animation-name: rotateInUpRight;\r\n}\r\n\r\n@-webkit-keyframes rotateOut {\r\n  from {\r\n    -webkit-transform-origin: center;\r\n    transform-origin: center;\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: center;\r\n    transform-origin: center;\r\n    -webkit-transform: rotate3d(0, 0, 1, 200deg);\r\n    transform: rotate3d(0, 0, 1, 200deg);\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n@keyframes rotateOut {\r\n  from {\r\n    -webkit-transform-origin: center;\r\n    transform-origin: center;\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: center;\r\n    transform-origin: center;\r\n    -webkit-transform: rotate3d(0, 0, 1, 200deg);\r\n    transform: rotate3d(0, 0, 1, 200deg);\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n.rotateOut {\r\n  -webkit-animation-name: rotateOut;\r\n  animation-name: rotateOut;\r\n}\r\n\r\n@-webkit-keyframes rotateOutDownLeft {\r\n  from {\r\n    -webkit-transform-origin: left bottom;\r\n    transform-origin: left bottom;\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: left bottom;\r\n    transform-origin: left bottom;\r\n    -webkit-transform: rotate3d(0, 0, 1, 45deg);\r\n    transform: rotate3d(0, 0, 1, 45deg);\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n@keyframes rotateOutDownLeft {\r\n  from {\r\n    -webkit-transform-origin: left bottom;\r\n    transform-origin: left bottom;\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: left bottom;\r\n    transform-origin: left bottom;\r\n    -webkit-transform: rotate3d(0, 0, 1, 45deg);\r\n    transform: rotate3d(0, 0, 1, 45deg);\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n.rotateOutDownLeft {\r\n  -webkit-animation-name: rotateOutDownLeft;\r\n  animation-name: rotateOutDownLeft;\r\n}\r\n\r\n@-webkit-keyframes rotateOutDownRight {\r\n  from {\r\n    -webkit-transform-origin: right bottom;\r\n    transform-origin: right bottom;\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: right bottom;\r\n    transform-origin: right bottom;\r\n    -webkit-transform: rotate3d(0, 0, 1, -45deg);\r\n    transform: rotate3d(0, 0, 1, -45deg);\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n@keyframes rotateOutDownRight {\r\n  from {\r\n    -webkit-transform-origin: right bottom;\r\n    transform-origin: right bottom;\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: right bottom;\r\n    transform-origin: right bottom;\r\n    -webkit-transform: rotate3d(0, 0, 1, -45deg);\r\n    transform: rotate3d(0, 0, 1, -45deg);\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n.rotateOutDownRight {\r\n  -webkit-animation-name: rotateOutDownRight;\r\n  animation-name: rotateOutDownRight;\r\n}\r\n\r\n@-webkit-keyframes rotateOutUpLeft {\r\n  from {\r\n    -webkit-transform-origin: left bottom;\r\n    transform-origin: left bottom;\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: left bottom;\r\n    transform-origin: left bottom;\r\n    -webkit-transform: rotate3d(0, 0, 1, -45deg);\r\n    transform: rotate3d(0, 0, 1, -45deg);\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n@keyframes rotateOutUpLeft {\r\n  from {\r\n    -webkit-transform-origin: left bottom;\r\n    transform-origin: left bottom;\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: left bottom;\r\n    transform-origin: left bottom;\r\n    -webkit-transform: rotate3d(0, 0, 1, -45deg);\r\n    transform: rotate3d(0, 0, 1, -45deg);\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n.rotateOutUpLeft {\r\n  -webkit-animation-name: rotateOutUpLeft;\r\n  animation-name: rotateOutUpLeft;\r\n}\r\n\r\n@-webkit-keyframes rotateOutUpRight {\r\n  from {\r\n    -webkit-transform-origin: right bottom;\r\n    transform-origin: right bottom;\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: right bottom;\r\n    transform-origin: right bottom;\r\n    -webkit-transform: rotate3d(0, 0, 1, 90deg);\r\n    transform: rotate3d(0, 0, 1, 90deg);\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n@keyframes rotateOutUpRight {\r\n  from {\r\n    -webkit-transform-origin: right bottom;\r\n    transform-origin: right bottom;\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform-origin: right bottom;\r\n    transform-origin: right bottom;\r\n    -webkit-transform: rotate3d(0, 0, 1, 90deg);\r\n    transform: rotate3d(0, 0, 1, 90deg);\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n.rotateOutUpRight {\r\n  -webkit-animation-name: rotateOutUpRight;\r\n  animation-name: rotateOutUpRight;\r\n}\r\n\r\n@-webkit-keyframes hinge {\r\n  0% {\r\n    -webkit-transform-origin: top left;\r\n    transform-origin: top left;\r\n    -webkit-animation-timing-function: ease-in-out;\r\n    animation-timing-function: ease-in-out;\r\n  }\r\n\r\n  20%,\r\n  60% {\r\n    -webkit-transform: rotate3d(0, 0, 1, 80deg);\r\n    transform: rotate3d(0, 0, 1, 80deg);\r\n    -webkit-transform-origin: top left;\r\n    transform-origin: top left;\r\n    -webkit-animation-timing-function: ease-in-out;\r\n    animation-timing-function: ease-in-out;\r\n  }\r\n\r\n  40%,\r\n  80% {\r\n    -webkit-transform: rotate3d(0, 0, 1, 60deg);\r\n    transform: rotate3d(0, 0, 1, 60deg);\r\n    -webkit-transform-origin: top left;\r\n    transform-origin: top left;\r\n    -webkit-animation-timing-function: ease-in-out;\r\n    animation-timing-function: ease-in-out;\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 700px, 0);\r\n    transform: translate3d(0, 700px, 0);\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n@keyframes hinge {\r\n  0% {\r\n    -webkit-transform-origin: top left;\r\n    transform-origin: top left;\r\n    -webkit-animation-timing-function: ease-in-out;\r\n    animation-timing-function: ease-in-out;\r\n  }\r\n\r\n  20%,\r\n  60% {\r\n    -webkit-transform: rotate3d(0, 0, 1, 80deg);\r\n    transform: rotate3d(0, 0, 1, 80deg);\r\n    -webkit-transform-origin: top left;\r\n    transform-origin: top left;\r\n    -webkit-animation-timing-function: ease-in-out;\r\n    animation-timing-function: ease-in-out;\r\n  }\r\n\r\n  40%,\r\n  80% {\r\n    -webkit-transform: rotate3d(0, 0, 1, 60deg);\r\n    transform: rotate3d(0, 0, 1, 60deg);\r\n    -webkit-transform-origin: top left;\r\n    transform-origin: top left;\r\n    -webkit-animation-timing-function: ease-in-out;\r\n    animation-timing-function: ease-in-out;\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 700px, 0);\r\n    transform: translate3d(0, 700px, 0);\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n.hinge {\r\n  -webkit-animation-duration: 2s;\r\n  animation-duration: 2s;\r\n  -webkit-animation-name: hinge;\r\n  animation-name: hinge;\r\n}\r\n\r\n@-webkit-keyframes jackInTheBox {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: scale(0.1) rotate(30deg);\r\n    transform: scale(0.1) rotate(30deg);\r\n    -webkit-transform-origin: center bottom;\r\n    transform-origin: center bottom;\r\n  }\r\n\r\n  50% {\r\n    -webkit-transform: rotate(-10deg);\r\n    transform: rotate(-10deg);\r\n  }\r\n\r\n  70% {\r\n    -webkit-transform: rotate(3deg);\r\n    transform: rotate(3deg);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: scale(1);\r\n    transform: scale(1);\r\n  }\r\n}\r\n\r\n@keyframes jackInTheBox {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: scale(0.1) rotate(30deg);\r\n    transform: scale(0.1) rotate(30deg);\r\n    -webkit-transform-origin: center bottom;\r\n    transform-origin: center bottom;\r\n  }\r\n\r\n  50% {\r\n    -webkit-transform: rotate(-10deg);\r\n    transform: rotate(-10deg);\r\n  }\r\n\r\n  70% {\r\n    -webkit-transform: rotate(3deg);\r\n    transform: rotate(3deg);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: scale(1);\r\n    transform: scale(1);\r\n  }\r\n}\r\n\r\n.jackInTheBox {\r\n  -webkit-animation-name: jackInTheBox;\r\n  animation-name: jackInTheBox;\r\n}\r\n\r\n/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */\r\n\r\n@-webkit-keyframes rollIn {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg);\r\n    transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes rollIn {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg);\r\n    transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg);\r\n  }\r\n\r\n  to {\r\n    opacity: 1;\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.rollIn {\r\n  -webkit-animation-name: rollIn;\r\n  animation-name: rollIn;\r\n}\r\n\r\n/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */\r\n\r\n@-webkit-keyframes rollOut {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg);\r\n    transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg);\r\n  }\r\n}\r\n\r\n@keyframes rollOut {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg);\r\n    transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg);\r\n  }\r\n}\r\n\r\n.rollOut {\r\n  -webkit-animation-name: rollOut;\r\n  animation-name: rollOut;\r\n}\r\n\r\n@-webkit-keyframes zoomIn {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\r\n    transform: scale3d(0.3, 0.3, 0.3);\r\n  }\r\n\r\n  50% {\r\n    opacity: 1;\r\n  }\r\n}\r\n\r\n@keyframes zoomIn {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\r\n    transform: scale3d(0.3, 0.3, 0.3);\r\n  }\r\n\r\n  50% {\r\n    opacity: 1;\r\n  }\r\n}\r\n\r\n.zoomIn {\r\n  -webkit-animation-name: zoomIn;\r\n  animation-name: zoomIn;\r\n}\r\n\r\n@-webkit-keyframes zoomInDown {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -1000px, 0);\r\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -1000px, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n  }\r\n\r\n  60% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\r\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n  }\r\n}\r\n\r\n@keyframes zoomInDown {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -1000px, 0);\r\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -1000px, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n  }\r\n\r\n  60% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\r\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n  }\r\n}\r\n\r\n.zoomInDown {\r\n  -webkit-animation-name: zoomInDown;\r\n  animation-name: zoomInDown;\r\n}\r\n\r\n@-webkit-keyframes zoomInLeft {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(-1000px, 0, 0);\r\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(-1000px, 0, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n  }\r\n\r\n  60% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(10px, 0, 0);\r\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(10px, 0, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n  }\r\n}\r\n\r\n@keyframes zoomInLeft {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(-1000px, 0, 0);\r\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(-1000px, 0, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n  }\r\n\r\n  60% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(10px, 0, 0);\r\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(10px, 0, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n  }\r\n}\r\n\r\n.zoomInLeft {\r\n  -webkit-animation-name: zoomInLeft;\r\n  animation-name: zoomInLeft;\r\n}\r\n\r\n@-webkit-keyframes zoomInRight {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(1000px, 0, 0);\r\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(1000px, 0, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n  }\r\n\r\n  60% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(-10px, 0, 0);\r\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(-10px, 0, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n  }\r\n}\r\n\r\n@keyframes zoomInRight {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(1000px, 0, 0);\r\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(1000px, 0, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n  }\r\n\r\n  60% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(-10px, 0, 0);\r\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(-10px, 0, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n  }\r\n}\r\n\r\n.zoomInRight {\r\n  -webkit-animation-name: zoomInRight;\r\n  animation-name: zoomInRight;\r\n}\r\n\r\n@-webkit-keyframes zoomInUp {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 1000px, 0);\r\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 1000px, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n  }\r\n\r\n  60% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\r\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n  }\r\n}\r\n\r\n@keyframes zoomInUp {\r\n  from {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 1000px, 0);\r\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 1000px, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n  }\r\n\r\n  60% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\r\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n  }\r\n}\r\n\r\n.zoomInUp {\r\n  -webkit-animation-name: zoomInUp;\r\n  animation-name: zoomInUp;\r\n}\r\n\r\n@-webkit-keyframes zoomOut {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  50% {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\r\n    transform: scale3d(0.3, 0.3, 0.3);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n@keyframes zoomOut {\r\n  from {\r\n    opacity: 1;\r\n  }\r\n\r\n  50% {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\r\n    transform: scale3d(0.3, 0.3, 0.3);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n  }\r\n}\r\n\r\n.zoomOut {\r\n  -webkit-animation-name: zoomOut;\r\n  animation-name: zoomOut;\r\n}\r\n\r\n@-webkit-keyframes zoomOutDown {\r\n  40% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\r\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 2000px, 0);\r\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 2000px, 0);\r\n    -webkit-transform-origin: center bottom;\r\n    transform-origin: center bottom;\r\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n  }\r\n}\r\n\r\n@keyframes zoomOutDown {\r\n  40% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\r\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 2000px, 0);\r\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 2000px, 0);\r\n    -webkit-transform-origin: center bottom;\r\n    transform-origin: center bottom;\r\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n  }\r\n}\r\n\r\n.zoomOutDown {\r\n  -webkit-animation-name: zoomOutDown;\r\n  animation-name: zoomOutDown;\r\n}\r\n\r\n@-webkit-keyframes zoomOutLeft {\r\n  40% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(42px, 0, 0);\r\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(42px, 0, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: scale(0.1) translate3d(-2000px, 0, 0);\r\n    transform: scale(0.1) translate3d(-2000px, 0, 0);\r\n    -webkit-transform-origin: left center;\r\n    transform-origin: left center;\r\n  }\r\n}\r\n\r\n@keyframes zoomOutLeft {\r\n  40% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(42px, 0, 0);\r\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(42px, 0, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: scale(0.1) translate3d(-2000px, 0, 0);\r\n    transform: scale(0.1) translate3d(-2000px, 0, 0);\r\n    -webkit-transform-origin: left center;\r\n    transform-origin: left center;\r\n  }\r\n}\r\n\r\n.zoomOutLeft {\r\n  -webkit-animation-name: zoomOutLeft;\r\n  animation-name: zoomOutLeft;\r\n}\r\n\r\n@-webkit-keyframes zoomOutRight {\r\n  40% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(-42px, 0, 0);\r\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(-42px, 0, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: scale(0.1) translate3d(2000px, 0, 0);\r\n    transform: scale(0.1) translate3d(2000px, 0, 0);\r\n    -webkit-transform-origin: right center;\r\n    transform-origin: right center;\r\n  }\r\n}\r\n\r\n@keyframes zoomOutRight {\r\n  40% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(-42px, 0, 0);\r\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(-42px, 0, 0);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: scale(0.1) translate3d(2000px, 0, 0);\r\n    transform: scale(0.1) translate3d(2000px, 0, 0);\r\n    -webkit-transform-origin: right center;\r\n    transform-origin: right center;\r\n  }\r\n}\r\n\r\n.zoomOutRight {\r\n  -webkit-animation-name: zoomOutRight;\r\n  animation-name: zoomOutRight;\r\n}\r\n\r\n@-webkit-keyframes zoomOutUp {\r\n  40% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\r\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -2000px, 0);\r\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -2000px, 0);\r\n    -webkit-transform-origin: center bottom;\r\n    transform-origin: center bottom;\r\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n  }\r\n}\r\n\r\n@keyframes zoomOutUp {\r\n  40% {\r\n    opacity: 1;\r\n    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\r\n    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);\r\n    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\r\n  }\r\n\r\n  to {\r\n    opacity: 0;\r\n    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -2000px, 0);\r\n    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -2000px, 0);\r\n    -webkit-transform-origin: center bottom;\r\n    transform-origin: center bottom;\r\n    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);\r\n  }\r\n}\r\n\r\n.zoomOutUp {\r\n  -webkit-animation-name: zoomOutUp;\r\n  animation-name: zoomOutUp;\r\n}\r\n\r\n@-webkit-keyframes slideInDown {\r\n  from {\r\n    -webkit-transform: translate3d(0, -100%, 0);\r\n    transform: translate3d(0, -100%, 0);\r\n    visibility: visible;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes slideInDown {\r\n  from {\r\n    -webkit-transform: translate3d(0, -100%, 0);\r\n    transform: translate3d(0, -100%, 0);\r\n    visibility: visible;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.slideInDown {\r\n  -webkit-animation-name: slideInDown;\r\n  animation-name: slideInDown;\r\n}\r\n\r\n@-webkit-keyframes slideInLeft {\r\n  from {\r\n    -webkit-transform: translate3d(-100%, 0, 0);\r\n    transform: translate3d(-100%, 0, 0);\r\n    visibility: visible;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes slideInLeft {\r\n  from {\r\n    -webkit-transform: translate3d(-100%, 0, 0);\r\n    transform: translate3d(-100%, 0, 0);\r\n    visibility: visible;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.slideInLeft {\r\n  -webkit-animation-name: slideInLeft;\r\n  animation-name: slideInLeft;\r\n}\r\n\r\n@-webkit-keyframes slideInRight {\r\n  from {\r\n    -webkit-transform: translate3d(100%, 0, 0);\r\n    transform: translate3d(100%, 0, 0);\r\n    visibility: visible;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes slideInRight {\r\n  from {\r\n    -webkit-transform: translate3d(100%, 0, 0);\r\n    transform: translate3d(100%, 0, 0);\r\n    visibility: visible;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.slideInRight {\r\n  -webkit-animation-name: slideInRight;\r\n  animation-name: slideInRight;\r\n}\r\n\r\n@-webkit-keyframes slideInUp {\r\n  from {\r\n    -webkit-transform: translate3d(0, 100%, 0);\r\n    transform: translate3d(0, 100%, 0);\r\n    visibility: visible;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes slideInUp {\r\n  from {\r\n    -webkit-transform: translate3d(0, 100%, 0);\r\n    transform: translate3d(0, 100%, 0);\r\n    visibility: visible;\r\n  }\r\n\r\n  to {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n}\r\n\r\n.slideInUp {\r\n  -webkit-animation-name: slideInUp;\r\n  animation-name: slideInUp;\r\n}\r\n\r\n@-webkit-keyframes slideOutDown {\r\n  from {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n\r\n  to {\r\n    visibility: hidden;\r\n    -webkit-transform: translate3d(0, 100%, 0);\r\n    transform: translate3d(0, 100%, 0);\r\n  }\r\n}\r\n\r\n@keyframes slideOutDown {\r\n  from {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n\r\n  to {\r\n    visibility: hidden;\r\n    -webkit-transform: translate3d(0, 100%, 0);\r\n    transform: translate3d(0, 100%, 0);\r\n  }\r\n}\r\n\r\n.slideOutDown {\r\n  -webkit-animation-name: slideOutDown;\r\n  animation-name: slideOutDown;\r\n}\r\n\r\n@-webkit-keyframes slideOutLeft {\r\n  from {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n\r\n  to {\r\n    visibility: hidden;\r\n    -webkit-transform: translate3d(-100%, 0, 0);\r\n    transform: translate3d(-100%, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes slideOutLeft {\r\n  from {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n\r\n  to {\r\n    visibility: hidden;\r\n    -webkit-transform: translate3d(-100%, 0, 0);\r\n    transform: translate3d(-100%, 0, 0);\r\n  }\r\n}\r\n\r\n.slideOutLeft {\r\n  -webkit-animation-name: slideOutLeft;\r\n  animation-name: slideOutLeft;\r\n}\r\n\r\n@-webkit-keyframes slideOutRight {\r\n  from {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n\r\n  to {\r\n    visibility: hidden;\r\n    -webkit-transform: translate3d(100%, 0, 0);\r\n    transform: translate3d(100%, 0, 0);\r\n  }\r\n}\r\n\r\n@keyframes slideOutRight {\r\n  from {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n\r\n  to {\r\n    visibility: hidden;\r\n    -webkit-transform: translate3d(100%, 0, 0);\r\n    transform: translate3d(100%, 0, 0);\r\n  }\r\n}\r\n\r\n.slideOutRight {\r\n  -webkit-animation-name: slideOutRight;\r\n  animation-name: slideOutRight;\r\n}\r\n\r\n@-webkit-keyframes slideOutUp {\r\n  from {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n\r\n  to {\r\n    visibility: hidden;\r\n    -webkit-transform: translate3d(0, -100%, 0);\r\n    transform: translate3d(0, -100%, 0);\r\n  }\r\n}\r\n\r\n@keyframes slideOutUp {\r\n  from {\r\n    -webkit-transform: translate3d(0, 0, 0);\r\n    transform: translate3d(0, 0, 0);\r\n  }\r\n\r\n  to {\r\n    visibility: hidden;\r\n    -webkit-transform: translate3d(0, -100%, 0);\r\n    transform: translate3d(0, -100%, 0);\r\n  }\r\n}\r\n\r\n.slideOutUp {\r\n  -webkit-animation-name: slideOutUp;\r\n  animation-name: slideOutUp;\r\n}\r\n\r\n.animated {\r\n  -webkit-animation-duration: 1s;\r\n  animation-duration: 1s;\r\n  -webkit-animation-fill-mode: both;\r\n  animation-fill-mode: both;\r\n}\r\n\r\n.animated.infinite {\r\n  -webkit-animation-iteration-count: infinite;\r\n  animation-iteration-count: infinite;\r\n}\r\n\r\n.animated.delay-1s {\r\n  -webkit-animation-delay: 1s;\r\n  animation-delay: 1s;\r\n}\r\n\r\n.animated.delay-2s {\r\n  -webkit-animation-delay: 2s;\r\n  animation-delay: 2s;\r\n}\r\n\r\n.animated.delay-3s {\r\n  -webkit-animation-delay: 3s;\r\n  animation-delay: 3s;\r\n}\r\n\r\n.animated.delay-4s {\r\n  -webkit-animation-delay: 4s;\r\n  animation-delay: 4s;\r\n}\r\n\r\n.animated.delay-5s {\r\n  -webkit-animation-delay: 5s;\r\n  animation-delay: 5s;\r\n}\r\n\r\n.animated.fast {\r\n  -webkit-animation-duration: 800ms;\r\n  animation-duration: 800ms;\r\n}\r\n\r\n.animated.faster {\r\n  -webkit-animation-duration: 500ms;\r\n  animation-duration: 500ms;\r\n}\r\n\r\n.animated.slow {\r\n  -webkit-animation-duration: 2s;\r\n  animation-duration: 2s;\r\n}\r\n\r\n.animated.slower {\r\n  -webkit-animation-duration: 3s;\r\n  animation-duration: 3s;\r\n}\r\n\r\n@media (prefers-reduced-motion) {\r\n  .animated {\r\n    -webkit-animation: unset !important;\r\n    animation: unset !important;\r\n    -webkit-transition: none !important;\r\n    transition: none !important;\r\n  }\r\n}", ""]);

// exports


/***/ }),

/***/ "../node_modules/nativescript-dev-webpack/style-hot-loader.js!../node_modules/nativescript-dev-webpack/apply-css-loader.js!../node_modules/css-loader/index.js?!../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../node_modules/vue-loader/lib/index.js?!./components/App.vue?vue&type=style&index=0&id=45ba5ed4&scoped=true&lang=css&":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.searching[data-v-45ba5ed4] {\n  z-index: 100000;\n}\n.header[data-v-45ba5ed4] {\n  margin: 25 10 10;\n  height: 30;\n}\n.footer[data-v-45ba5ed4] {\n  padding: 10 50 10;\n  height: 50;\n}\n#view[data-v-45ba5ed4] {\n  vertical-align: center;\n}\n#app[data-v-45ba5ed4] {\n  z-index: 10;\n}\n#earth[data-v-45ba5ed4] {\n  z-index: 5;\n  animation-name: bounceInOpacity;\n  animation-duration: 1s;\n  animation-delay: 0.5s;\n}\n#background[data-v-45ba5ed4] {\n  width: 100;\n  height: 100;\n  border-radius: 100%;\n}\n#background.secondary[data-v-45ba5ed4] {\n  background: linear-gradient(-45deg, #ff067f, #ff0154);\n  animation-name: circle-data-v-45ba5ed4;\n  animation-duration: 1s;\n  animation-fill-mode: forwards;\n}\n@keyframes circle-data-v-45ba5ed4 {\nfrom {\n    transform: scale(0);\n}\nto {\n    transform: scale(10);\n}\n}\n", ""]);

// exports

    const application = __webpack_require__("../node_modules/tns-core-modules/application/application.js");
    __webpack_require__("../node_modules/tns-core-modules/ui/styling/style-scope.js");

    exports.forEach(cssExport => {
        if (cssExport.length > 1 && cssExport[1]) {
            // applying the second item of the export as it contains the css contents
            application.addCss(cssExport[1]);
        }
    });
    ;
    if (false) {}


/***/ }),

/***/ "../node_modules/nativescript-dev-webpack/style-hot-loader.js!../node_modules/nativescript-dev-webpack/apply-css-loader.js!../node_modules/css-loader/index.js?!../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../node_modules/vue-loader/lib/index.js?!./components/Card.vue?vue&type=style&index=0&id=f883d62a&scoped=true&lang=css&":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.card .background[data-v-f883d62a] {\n    transform: scale(1.05);\n    opacity: 0.75;\n}\n@keyframes highlight-data-v-f883d62a {\nfrom {\n        transform: scale(0.85);\n}\nto {\n        transform: scale(1);\n}\n}\n@keyframes downlight-data-v-f883d62a {\nfrom {\n        transform: scale(1);\n}\nto {\n        transform: scale(0.85);\n}\n}\n.card[data-v-f883d62a] {\n    background-position: center center;\n    background-size: cover;\n    background-repeat: no-repeat;\n    transform: scale(0.85);\n}\n.card.active[data-v-f883d62a] {\n    animation-name: highlight-data-v-f883d62a;\n    animation-duration: 0.25s;\n    animation-fill-mode: forwards;\n}\n.card.inactive[data-v-f883d62a] {\n    animation-name: downlight-data-v-f883d62a;\n    animation-duration: 0.25s;\n    animation-fill-mode: backwards;\n}\n.card .head[data-v-f883d62a] {\n    border-radius: 14 14 0 0;\n    background: #ffffff;\n    padding: 12;\n}\n.card .img[data-v-f883d62a] {\n    background-position: center center;\n    background-size: cover;\n    background-repeat: no-repeat;\n    border-radius: 0 0 14 14;\n}\n.card .title[data-v-f883d62a] {\n    color: #545454;\n    font-size: 18;\n}\n.card .subTitle[data-v-f883d62a] {\n    color: #DADADA;\n    font-size: 16;\n}\n", ""]);

// exports

    const application = __webpack_require__("../node_modules/tns-core-modules/application/application.js");
    __webpack_require__("../node_modules/tns-core-modules/ui/styling/style-scope.js");

    exports.forEach(cssExport => {
        if (cssExport.length > 1 && cssExport[1]) {
            // applying the second item of the export as it contains the css contents
            application.addCss(cssExport[1]);
        }
    });
    ;
    if (false) {}


/***/ }),

/***/ "../node_modules/nativescript-dev-webpack/style-hot-loader.js!../node_modules/nativescript-dev-webpack/apply-css-loader.js!../node_modules/css-loader/index.js?!../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../node_modules/vue-loader/lib/index.js?!./components/CityOverview.vue?vue&type=style&index=0&id=04915cdf&scoped=true&lang=css&":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.header[data-v-04915cdf] {\n  margin: 25 10 10;\n  height: 30;\n}\n.footer[data-v-04915cdf] {\n  margin: 10 50 10;\n  height: 30;\n}\n#overview #view[data-v-04915cdf] {\n  opacity: 0;\n  animation-name: fadeInRight;\n  animation-duration: 0.5s;\n  animation-fill-mode: forwards;\n  animation-delay: 0.5s;\n}\n#overview .h1[data-v-04915cdf] {\n  color: #545454;\n  font-size: 40;\n  margin-bottom: 5;\n  opacity: 0;\n  animation-name: fadeInDown;\n  animation-duration: 0.5s;\n  animation-fill-mode: forwards;\n  animation-delay: 0.5s;\n}\n#overview .description-label[data-v-04915cdf] {\n  color: #dadada;\n  opacity: 0;\n  animation-name: fadeInUp;\n  animation-duration: 0.5s;\n  animation-fill-mode: forwards;\n  animation-delay: 0.75s;\n}\n#view[data-v-04915cdf] {\n  vertical-align: center;\n}\n", ""]);

// exports

    const application = __webpack_require__("../node_modules/tns-core-modules/application/application.js");
    __webpack_require__("../node_modules/tns-core-modules/ui/styling/style-scope.js");

    exports.forEach(cssExport => {
        if (cssExport.length > 1 && cssExport[1]) {
            // applying the second item of the export as it contains the css contents
            application.addCss(cssExport[1]);
        }
    });
    ;
    if (false) {}


/***/ }),

/***/ "../node_modules/nativescript-dev-webpack/style-hot-loader.js!../node_modules/nativescript-dev-webpack/apply-css-loader.js!../node_modules/css-loader/index.js?!../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../node_modules/vue-loader/lib/index.js?!./components/Header.vue?vue&type=style&index=0&id=1a9bb128&scoped=true&lang=css&":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n#header[data-v-1a9bb128] {\n  padding: 0;\n}\n#header .btn[data-v-1a9bb128] {\n  margin: 0;\n  padding: 0;\n  justify-content: center;\n  align-items: center;\n}\n#header .btn .icon[data-v-1a9bb128] {\n  padding: 0;\n  margin: 0;\n}\n#header .btn.pp[data-v-1a9bb128] {\n  height: 100%;\n}\n#header .btn.menu .icon[data-v-1a9bb128] {\n  height: 60%;\n}\n#header .btn.pp .icon[data-v-1a9bb128] {\n  border: 2px solid #ffffff;\n  border-radius: 100%;\n}\n", ""]);

// exports

    const application = __webpack_require__("../node_modules/tns-core-modules/application/application.js");
    __webpack_require__("../node_modules/tns-core-modules/ui/styling/style-scope.js");

    exports.forEach(cssExport => {
        if (cssExport.length > 1 && cssExport[1]) {
            // applying the second item of the export as it contains the css contents
            application.addCss(cssExport[1]);
        }
    });
    ;
    if (false) {}


/***/ }),

/***/ "../node_modules/nativescript-dev-webpack/style-hot-loader.js!../node_modules/nativescript-dev-webpack/apply-css-loader.js!../node_modules/css-loader/index.js?!../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../node_modules/vue-loader/lib/index.js?!./components/Home.vue?vue&type=style&index=0&id=67410f3a&scoped=true&lang=css&":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n#home .hr[data-v-67410f3a] {\n  background: linear-gradient(-45deg, #ff067f, #ff0154);\n  border-radius: 16px;\n  margin-bottom: 25;\n  opacity: 0;\n  animation-name: bounceIn;\n  animation-duration: 0.5s;\n  animation-fill-mode: forwards;\n  animation-delay: 0.5s;\n}\n#home .h1[data-v-67410f3a] {\n  font-size: 40;\n  margin-bottom: 25;\n  opacity: 0;\n  animation-name: fadeInDown;\n  animation-duration: 0.5s;\n  animation-fill-mode: forwards;\n  animation-delay: 0.5s;\n}\n#home .h2[data-v-67410f3a] {\n  font-size: 18;\n  margin-bottom: 25;\n  opacity: 0;\n  animation-name: fadeInUp;\n  animation-duration: 0.5s;\n  animation-fill-mode: forwards;\n  animation-delay: 0.5s;\n}\n#home.h1[data-v-67410f3a],\n#home .h2[data-v-67410f3a] {\n  color: #545454;\n}\n#home .h3[data-v-67410f3a] {\n  color: #dadada;\n}\n#home .search-btn[data-v-67410f3a] {\n  opacity: 0;\n  animation-name: bounceIn;\n  animation-duration: 0.5s;\n  animation-fill-mode: forwards;\n  animation-delay: 1s;\n  margin-bottom: 25;\n  margin-top: 25;\n}\n#home .search-btn .icon[data-v-67410f3a] {\n  opacity: 0;\n  animation-name: bounceIn;\n  animation-duration: 0.5s;\n  animation-fill-mode: forwards;\n  animation-delay: 1.25s;\n  z-index: 100000;\n}\n#home .search-btn .background[data-v-67410f3a] {\n  width: 100;\n  height: 100;\n  border-radius: 16;\n  background: linear-gradient(-45deg, #ff067f, #ff0154);\n}\n#home .description-label[data-v-67410f3a] {\n  opacity: 0;\n  animation-name: bounceIn;\n  animation-duration: 1s;\n  animation-fill-mode: forwards;\n  animation-delay: 1.5s;\n  margin-bottom: 15;\n}\n", ""]);

// exports

    const application = __webpack_require__("../node_modules/tns-core-modules/application/application.js");
    __webpack_require__("../node_modules/tns-core-modules/ui/styling/style-scope.js");

    exports.forEach(cssExport => {
        if (cssExport.length > 1 && cssExport[1]) {
            // applying the second item of the export as it contains the css contents
            application.addCss(cssExport[1]);
        }
    });
    ;
    if (false) {}


/***/ }),

/***/ "../node_modules/nativescript-dev-webpack/style-hot-loader.js!../node_modules/nativescript-dev-webpack/apply-css-loader.js!../node_modules/css-loader/index.js?!../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../node_modules/vue-loader/lib/index.js?!./components/Place.vue?vue&type=style&index=0&id=7012a93c&scoped=true&lang=css&":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.image[data-v-7012a93c] {\n  background-size: cover;\n}\n.desc[data-v-7012a93c] {\n  text-justify: justify;\n  font-size: 14;\n  color: #545454;\n  background-color: transparent;\n  border-color: transparent;\n}\n.text[data-v-7012a93c] {\n  margin: 0 25;\n}\n.text .title[data-v-7012a93c] {\n  margin-top: 8;\n}\n.text .desc[data-v-7012a93c] {\n  margin-top: 16;\n}\n.actions[data-v-7012a93c] {\n  /* background-color: blue; */\n  z-index: 10;\n  left: 0;\n  top: 170;\n  padding: 0 25;\n}\n.actions .ac-btn[data-v-7012a93c] {\n  border-radius: 100%;\n  margin-right: 8;\n  background: linear-gradient(-45deg, #ff067f, #ff0154);\n}\n.actions #star[data-v-7012a93c] {\n  opacity: 0;\n  animation-name: bounceIn;\n  animation-duration: 0.5s;\n  animation-fill-mode: forwards;\n  animation-delay: 0.75s;\n}\n.actions #plus[data-v-7012a93c] {\n  opacity: 0;\n  animation-name: bounceIn;\n  animation-duration: 0.5s;\n  animation-fill-mode: forwards;\n  animation-delay: 1s;\n}\n.header[data-v-7012a93c] {\n  margin: 25 10 10;\n  height: 32;\n}\n.footer[data-v-7012a93c] {\n  margin: 10 50 10;\n  height: 32;\n}\n.head[data-v-7012a93c] {\n  padding: 0 25;\n  margin-bottom: 25;\n  margin-top: 16;\n  opacity: 0;\n  animation-name: fadeInUp;\n  animation-duration: 1s;\n  animation-fill-mode: forwards;\n  animation-delay: 0.25s;\n}\n.title[data-v-7012a93c] {\n  color: #545454;\n  font-size: 28;\n  margin-bottom: 4;\n}\n.subTitle[data-v-7012a93c] {\n  color: #dadada;\n  font-size: 20;\n  margin-bottom: 10;\n}\n", ""]);

// exports

    const application = __webpack_require__("../node_modules/tns-core-modules/application/application.js");
    __webpack_require__("../node_modules/tns-core-modules/ui/styling/style-scope.js");

    exports.forEach(cssExport => {
        if (cssExport.length > 1 && cssExport[1]) {
            // applying the second item of the export as it contains the css contents
            application.addCss(cssExport[1]);
        }
    });
    ;
    if (false) {}


/***/ }),

/***/ "../node_modules/nativescript-dev-webpack/style-hot-loader.js!../node_modules/nativescript-dev-webpack/apply-css-loader.js!../node_modules/css-loader/index.js?!../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../node_modules/vue-loader/lib/index.js?!./components/Searching.vue?vue&type=style&index=0&id=1a2bcd22&scoped=true&lang=css&":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n#searching[data-v-1a2bcd22] {\n  padding: 0 25;\n  /* background:yellow; */\n}\n#searching .city[data-v-1a2bcd22] {\n  color: #ffffff;\n  font-size: 16;\n}\n#searching .h2[data-v-1a2bcd22] {\n  font-size: 18;\n  margin-bottom: 40;\n  color: #ffffff;\n}\n#searching .input-container[data-v-1a2bcd22] {\n  opacity: 0;\n  animation-name: bounceIn;\n  animation-duration: 1s;\n  animation-fill-mode: forwards;\n  animation-delay: 0s;\n}\n#searching .input[data-v-1a2bcd22] {\n  background-color: #ffffff;\n  height: 60;\n  text-decoration: none;\n  font-size: 35;\n  text-align: left;\n  width: 100%;\n  flex-grow: 2;\n}\n#searching .send-button[data-v-1a2bcd22] {\n  background-color: #ffffff;\n  height: 20;\n  text-decoration: none;\n  font-size: 20;\n\n  width: 100%;\n  opacity: 0;\n  animation-name: bounceIn;\n  animation-duration: 1s;\n  animation-fill-mode: forwards;\n  animation-delay: 0s;\n}\n#searching.ios .input[data-v-1a2bcd22] {\n  padding-top: 10;\n}\n#searching .input.empty[data-v-1a2bcd22] {\n  font-size: 16;\n  padding-top: 19;\n}\n#searching .close[data-v-1a2bcd22] {\n  height: 40;\n  width: 40;\n  background-image: \"~/assets/icons/Cross.png\";\n  background-size: contain;\n  margin-top: 75;\n  margin-bottom: 100;\n  opacity: 0;\n  animation-name: fadeIn;\n  animation-duration: 1s;\n  animation-fill-mode: forwards;\n  animation-delay: 0.5s;\n}\n", ""]);

// exports

    const application = __webpack_require__("../node_modules/tns-core-modules/application/application.js");
    __webpack_require__("../node_modules/tns-core-modules/ui/styling/style-scope.js");

    exports.forEach(cssExport => {
        if (cssExport.length > 1 && cssExport[1]) {
            // applying the second item of the export as it contains the css contents
            application.addCss(cssExport[1]);
        }
    });
    ;
    if (false) {}


/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/App.vue?vue&type=template&id=45ba5ed4&scoped=true&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "Page",
    { staticClass: "page", attrs: { actionBarHidden: "true" } },
    [
      _c(
        "ScrollView",
        [
          _c(
            "AbsoluteLayout",
            { attrs: { width: "100%", height: "100%" } },
            [
              _c(
                "FlexboxLayout",
                {
                  attrs: {
                    width: "100%",
                    height: "100%",
                    justifyContent: "center",
                    alignItems: "center"
                  }
                },
                [
                  _c("StackLayout", {
                    class: { secondary: _vm.isSearching },
                    attrs: { id: "background" }
                  })
                ],
                1
              ),
              _c(
                "FlexboxLayout",
                {
                  attrs: {
                    flexDirection: "column",
                    height: "100%",
                    width: "100%",
                    id: "app"
                  }
                },
                [
                  _c("Header", {
                    staticClass: "header",
                    attrs: { secondary: _vm.isSearching }
                  }),
                  _c(
                    "StackLayout",
                    {
                      staticClass: "home-panel",
                      attrs: { flexGrow: "7", id: "view" }
                    },
                    [
                      !_vm.isSearching
                        ? _c("Home", { on: { togglesearch: _vm.toggleSearch } })
                        : _c("Searching", {
                            staticClass: "searching",
                            on: { togglesearch: _vm.toggleSearch }
                          })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/Card.vue?vue&type=template&id=f883d62a&scoped=true&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "FlexboxLayout",
    {
      staticClass: "card",
      class: {
        active: _vm.index == _vm.active,
        inactive: _vm.index != _vm.active
      },
      attrs: { justifyContent: "space-between" },
      on: {
        tap: function($event) {
          _vm.goToPlace(_vm.place)
        }
      }
    },
    [
      _c(
        "AbsoluteLayout",
        { attrs: { width: "100%", height: "100%" } },
        [
          _c("Image", {
            staticClass: "background",
            attrs: {
              src: "~/assets/icons/box-shadow.png",
              height: "100%",
              width: "100%"
            }
          }),
          _c(
            "FlexboxLayout",
            {
              attrs: {
                flexDirection: "column",
                width: "96%",
                height: "97%",
                padding: "12 20 30 12"
              }
            },
            [
              _c(
                "StackLayout",
                { staticClass: "head", attrs: { orientation: "vertical" } },
                [
                  _c("Label", {
                    staticClass: "title bold",
                    attrs: { text: _vm.place.name }
                  }),
                  _c("Label", {
                    staticClass: "subTitle semi-bold",
                    attrs: { text: _vm.place.type }
                  }),
                  _c(
                    "FlexboxLayout",
                    {
                      attrs: {
                        flexDirection: "row",
                        height: "12",
                        marginTop: "4"
                      }
                    },
                    [
                      _vm._l(_vm.place.stars, function(star) {
                        return _c("Image", {
                          key: star,
                          attrs: {
                            src: "~/assets/icons/Star-Red.png",
                            marginRight: "4"
                          }
                        })
                      }),
                      _vm._l(5 - _vm.place.stars, function(star) {
                        return _c("Image", {
                          key: star * 10,
                          attrs: {
                            src: "~/assets/icons/Star-grey.png",
                            marginRight: "4"
                          }
                        })
                      })
                    ],
                    2
                  )
                ],
                1
              ),
              _c("StackLayout", {
                staticClass: "img",
                attrs: {
                  orientation: "vertical",
                  backgroundImage: "~/assets/media/" + _vm.place.media,
                  height: "58%",
                  width: "100%"
                }
              })
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CityOverview.vue?vue&type=template&id=04915cdf&scoped=true&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "Page",
    { staticClass: "page", attrs: { actionBarHidden: "true" } },
    [
      _c(
        "StackLayout",
        { attrs: { id: "overview" } },
        [
          _c(
            "FlexboxLayout",
            {
              attrs: { flexDirection: "column", height: "100%", width: "100%" }
            },
            [
              _c("Header", {
                staticClass: "header",
                attrs: { canBack: _vm.canBack }
              }),
              _c(
                "StackLayout",
                { attrs: { flexGrow: "1", id: "view" } },
                [
                  _c(
                    "FlexboxLayout",
                    { attrs: { flexDirection: "column", maxHeight: "100%" } },
                    [
                      _c(
                        "StackLayout",
                        {
                          attrs: { orientation: "vertical", margin: "0 50 0" }
                        },
                        [
                          _c("Label", {
                            staticClass: "h1 extra-bold",
                            attrs: { textWrap: "true", text: _vm.city }
                          }),
                          _c("Label", {
                            staticClass: "h3 description-label semi-bold",
                            attrs: {
                              textWrap: "true",
                              text: "Places you should visit"
                            }
                          })
                        ],
                        1
                      ),
                      _c(
                        "ScrollView",
                        {
                          ref: "list",
                          attrs: {
                            orientation: "horizontal",
                            marginTop: "24",
                            height: "calc(100% - 100)"
                          },
                          on: { scroll: _vm.getScroll }
                        },
                        [
                          _c(
                            "WrapLayout",
                            {
                              attrs: {
                                orientation: "horizontal",
                                paddingLeft: (_vm.width - 244) / 2 + 8,
                                paddingRight: (_vm.width - 244) / 2 - 8,
                                paddingBottom: "8"
                              }
                            },
                            _vm._l(_vm.places, function(place, index) {
                              return _c("Card", {
                                key: index,
                                attrs: {
                                  place: place,
                                  index: index,
                                  active: _vm.active,
                                  width: "244",
                                  height: "315"
                                }
                              })
                            })
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/Header.vue?vue&type=template&id=1a9bb128&scoped=true&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "FlexboxLayout",
    {
      attrs: {
        flexDirection: "row",
        justifyContent: "space-between",
        id: "header"
      }
    },
    [
      _c(
        "FlexboxLayout",
        { staticClass: "btn menu" },
        [
          _vm.canBack == true
            ? _c("Image", {
                staticClass: "icon",
                attrs: { src: "~/assets/icons/arrow.png" },
                on: { tap: _vm.$navigateBack }
              })
            : _vm.secondary == false
            ? _c("Image", {
                staticClass: "icon",
                attrs: { src: "~/assets/icons/Menu-Red.png" }
              })
            : _c("Image", {
                staticClass: "icon",
                attrs: { src: "~/assets/icons/Menu-White.png" }
              })
        ],
        1
      ),
      _c("FlexboxLayout", { staticClass: "btn pp" })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/Home.vue?vue&type=template&id=67410f3a&scoped=true&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "StackLayout",
    { attrs: { id: "home" } },
    [
      _c("Label", {
        staticClass: "h1 align-center extra-bold",
        attrs: { textWrap: "true", text: "Flamingo" }
      }),
      _c("Button", { staticClass: "hr", attrs: { width: "30", height: "5" } }),
      _c("Label", {
        staticClass: "h2 align-center bold",
        attrs: { textWrap: "true", text: "" }
      }),
      _c(
        "AbsoluteLayout",
        {
          staticClass: "search-btn",
          attrs: { width: "80", height: "80" },
          on: { tap: _vm.toggleSearch }
        },
        [
          _c("Image", {
            staticClass: "icon",
            attrs: {
              src: "~/assets/icons/Mail.png",
              left: "28",
              top: "25",
              width: "25",
              height: "25"
            }
          }),
          _c("Button", {
            staticClass: "background",
            attrs: { left: "0", top: "0", width: "100%", height: "100%" },
            on: { tap: _vm.toggleSearch }
          })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/Place.vue?vue&type=template&id=7012a93c&scoped=true&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "Page",
    { staticClass: "page", attrs: { actionBarHidden: "true" } },
    [
      _c(
        "StackLayout",
        { attrs: { orientation: "vertical" } },
        [
          _c(
            "AbsoluteLayout",
            { attrs: { width: "100%", height: "100%" } },
            [
              _c(
                "FlexboxLayout",
                {
                  attrs: {
                    flexDirection: "column",
                    height: "100%",
                    width: "100%",
                    id: "place"
                  }
                },
                [
                  _c("Header", {
                    staticClass: "header",
                    attrs: { canBack: _vm.canBack }
                  }),
                  _c(
                    "StackLayout",
                    { attrs: { width: "100%", height: "25", flexGrow: "7" } },
                    [
                      _c(
                        "ScrollView",
                        { attrs: { orientation: "vertical" } },
                        [
                          _c(
                            "StackLayout",
                            { attrs: { orientation: "vertical" } },
                            [
                              _c(
                                "StackLayout",
                                {
                                  staticClass: "head",
                                  attrs: { orientation: "vertical" }
                                },
                                [
                                  _c("Label", {
                                    staticClass: "title bold",
                                    attrs: { text: _vm.place.name }
                                  }),
                                  _c("Label", {
                                    staticClass: "subTitle semi-bold",
                                    attrs: { text: _vm.place.type }
                                  }),
                                  _c(
                                    "FlexboxLayout",
                                    {
                                      attrs: {
                                        flexDirection: "row",
                                        height: "12",
                                        marginTop: "4"
                                      }
                                    },
                                    [
                                      _vm._l(_vm.place.stars, function(star) {
                                        return _c("Image", {
                                          key: star,
                                          attrs: {
                                            src: "~/assets/icons/Star-Red.png",
                                            marginRight: "4"
                                          }
                                        })
                                      }),
                                      _vm._l(5 - _vm.place.stars, function(
                                        star
                                      ) {
                                        return _c("Image", {
                                          key: star * 10,
                                          attrs: {
                                            src: "~/assets/icons/Star-grey.png",
                                            marginRight: "4"
                                          }
                                        })
                                      })
                                    ],
                                    2
                                  )
                                ],
                                1
                              ),
                              _c(
                                "AbsoluteLayout",
                                { attrs: { width: "100%", height: "210" } },
                                [
                                  _c("StackLayout", {
                                    staticClass: "image",
                                    attrs: {
                                      top: "0",
                                      left: "0",
                                      height: 210 - 20,
                                      width: "100%",
                                      backgroundImage:
                                        "~/assets/media/" + _vm.place.media
                                    }
                                  }),
                                  _c(
                                    "StackLayout",
                                    {
                                      staticClass: "actions",
                                      attrs: {
                                        orientation: "horizontal",
                                        height: "40",
                                        width: "100%"
                                      }
                                    },
                                    [
                                      _c(
                                        "AbsoluteLayout",
                                        {
                                          staticClass: "ac-btn",
                                          attrs: {
                                            width: "40",
                                            height: "40",
                                            id: "star"
                                          }
                                        },
                                        [
                                          _c("Image", {
                                            attrs: {
                                              src:
                                                "~/assets/icons/Star-White.png",
                                              width: "15",
                                              height: "15",
                                              left: "12.5",
                                              top: "12.5"
                                            }
                                          })
                                        ],
                                        1
                                      ),
                                      _c(
                                        "AbsoluteLayout",
                                        {
                                          staticClass: "ac-btn",
                                          attrs: {
                                            width: "40",
                                            height: "40",
                                            id: "plus"
                                          }
                                        },
                                        [
                                          _c("Image", {
                                            attrs: {
                                              src: "~/assets/icons/Plus.png",
                                              width: "15",
                                              height: "15",
                                              left: "12.5",
                                              top: "12.5"
                                            }
                                          })
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _c(
                                "StackLayout",
                                {
                                  staticClass: "text",
                                  attrs: { orientation: "vertical" }
                                },
                                [
                                  _c("Label", {
                                    staticClass: "title bold",
                                    attrs: { text: "Lorem ipsum" }
                                  }),
                                  _c(
                                    "TextView",
                                    {
                                      staticClass: "desc semi-bold",
                                      attrs: {
                                        editable: "false",
                                        marginBottom: "25",
                                        padding: "0"
                                      }
                                    },
                                    [
                                      _c(
                                        "FormattedString",
                                        [
                                          _c("Span", {
                                            attrs: { text: _vm.lorem }
                                          })
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/Searching.vue?vue&type=template&id=1a2bcd22&scoped=true&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "StackLayout",
    {
      staticClass: "align-center",
      class: _vm.platform,
      attrs: { id: "searching" },
      on: { tap: _vm.clearFocus }
    },
    [
      _c("Label", { staticClass: "h2", attrs: { textWrap: "true", text: "" } }),
      _c(
        "FlexboxLayout",
        {
          staticClass: "input-container",
          attrs: {
            padding: "16",
            borderRadius: "16",
            backgroundColor: "#ffffff",
            width: "300",
            alignItems: "center",
            justifyContent: "center"
          }
        },
        [
          _c("Image", {
            attrs: {
              src: "~/assets/icons/Mail.png",
              height: "20",
              minWidth: "20",
              margin: "0 12"
            }
          }),
          _c("TextView", {
            ref: "searchBar",
            staticClass: "input extra-bold",
            class: { empty: _vm.searchQuery == "" },
            attrs: {
              width: _vm.searchBarWidth,
              editable: "true",
              hint: "Email",
              maxlength: "8",
              text: _vm.searchQuery
            },
            on: {
              textChange: function($event) {
                _vm.searchQuery = $event.value
              }
            }
          })
        ],
        1
      ),
      _c(
        "FlexboxLayout",
        {
          staticClass: "input-container",
          attrs: {
            marginTop: "10",
            padding: "12",
            borderRadius: "16",
            backgroundColor: "#ffffff",
            width: "300",
            alignItems: "center",
            justifyContent: "center"
          }
        },
        [_c("Button", { staticClass: "send-button bold" }, [_vm._v("Send")])],
        1
      ),
      _c(
        "AbsoluteLayout",
        {
          staticClass: "close",
          attrs: { width: "80", height: "80" },
          on: { tap: _vm.toggleSearch }
        },
        [
          _c("Image", {
            staticClass: "icon",
            attrs: {
              src: "~/assets/icons/Cross.png",
              left: "28",
              top: "25",
              width: "25",
              height: "25"
            }
          })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./ sync ^\\.\\/app\\.(css|scss|less|sass)$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./app.css": "./app.css"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./ sync ^\\.\\/app\\.(css|scss|less|sass)$";

/***/ }),

/***/ "./ sync recursive (root|page)\\.(xml|css|js|ts|scss)$":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	var e = new Error("Cannot find module '" + req + "'");
	e.code = 'MODULE_NOT_FOUND';
	throw e;
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "./ sync recursive (root|page)\\.(xml|css|js|ts|scss)$";

/***/ }),

/***/ "./app.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../node_modules/css-loader/lib/css-base.js")(false);
// imports
exports.i(__webpack_require__("../node_modules/css-loader/index.js?!../node_modules/nativescript-theme-core/css/core.light.css"), "");
exports.i(__webpack_require__("../node_modules/css-loader/index.js?!./css/animate.css"), "");

// module
exports.push([module.i, "/*\nIn NativeScript, the app.css file is where you place CSS rules that\nyou would like to apply to your entire application. Check out\nhttp://docs.nativescript.org/ui/styling for a full list of the CSS\nselectors and properties you can use to style UI components.\n\n/*\nIn many cases you may want to use the NativeScript core theme instead\nof writing your own CSS rules. For a full list of class names in the theme\nrefer to http://docs.nativescript.org/ui/theme.\nThe imported CSS rules must precede all other types of rules.\n*/\n\n\n.extra-bold{\n    font-family: \"Assistant-ExtraBold\",\"Assistant ExtraBold\";\n    /* font-family: 'Assistant', sans-serif; */\n    font-weight: 800;\n}\n\n.bold{\n    font-family: \"Assistant-Bold\",\"Assistant\";\n    /* font-family: 'Assistant', sans-serif; */\n    font-weight: 700;\n}\n\n.semi-bold{\n    font-family: \"Assistant-SemiBold\",\"Assistant SemiBold\";\n    /* font-family: 'Assistant', sans-serif; */\n    font-weight: 600;\n}\n\n.align-center { \n        text-align: center; \n}\n\n@keyframes bounceInOpacity {\n  from,\n  20%,\n  40%,\n  60%,\n  80%,\n  to {\n    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n\n  0% {\n    -webkit-transform: scale3d(0.3, 0.3, 0.3);\n    transform: scale3d(0.3, 0.3, 0.3);\n  }\n\n  20% {\n    -webkit-transform: scale3d(1.1, 1.1, 1.1);\n    transform: scale3d(1.1, 1.1, 1.1);\n  }\n\n  40% {\n    -webkit-transform: scale3d(0.9, 0.9, 0.9);\n    transform: scale3d(0.9, 0.9, 0.9);\n  }\n\n  60% {\n    -webkit-transform: scale3d(1.03, 1.03, 1.03);\n    transform: scale3d(1.03, 1.03, 1.03);\n  }\n\n  80% {\n    -webkit-transform: scale3d(0.97, 0.97, 0.97);\n    transform: scale3d(0.97, 0.97, 0.97);\n  }\n\n  to {\n    -webkit-transform: scale3d(1, 1, 1);\n    transform: scale3d(1, 1, 1);\n  }\n}", ""]);

// exports
;
    if (false) {}


/***/ }),

/***/ "./app.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/* harmony import */ var nativescript_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/nativescript-vue/dist/index.js");
/* harmony import */ var nativescript_vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(nativescript_vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_App__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/App.vue");
/* harmony import */ var _nativescript_orientation__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./nativescript-orientation/orientation.js");
/* harmony import */ var _nativescript_orientation__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_nativescript_orientation__WEBPACK_IMPORTED_MODULE_2__);

            __webpack_require__("../node_modules/nativescript-dev-webpack/load-application-css-regular.js")();
            
            
        if (false) {}
        
            const context = __webpack_require__("./ sync recursive (root|page)\\.(xml|css|js|ts|scss)$");
            global.registerWebpackModules(context);
            
        __webpack_require__("../node_modules/tns-core-modules/bundle-entry-points.js");
         // Vue.registerElement('CardView', () => require('./nativescript-cardview').CardView);

 //import App from './components/CityOverview';
// Uncommment the following to see NativeScript-Vue output logs

nativescript_vue__WEBPACK_IMPORTED_MODULE_0___default.a.config.silent = false;


const firebase = __webpack_require__("../node_modules/nativescript-plugin-firebase/firebase.js");

firebase.init({// Optionally pass in properties for database, authentication and cloud messaging,
  // see their respective docs.
}).then(() => {
  console.log("firebase.init done");
}, error => {
  console.log(`firebase.init error: ${error}`);
});
new nativescript_vue__WEBPACK_IMPORTED_MODULE_0___default.a({
  render: h => h('frame', [h(_components_App__WEBPACK_IMPORTED_MODULE_1__["default"])]),

  mounted() {
    setTimeout(() => {}, 0);
  }

}).$start();
    
        
        
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/nativescript-dev-webpack/node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./components/App.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _App_vue_vue_type_template_id_45ba5ed4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/App.vue?vue&type=template&id=45ba5ed4&scoped=true&");
/* harmony import */ var _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/App.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _App_vue_vue_type_style_index_0_id_45ba5ed4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./components/App.vue?vue&type=style&index=0&id=45ba5ed4&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _App_vue_vue_type_template_id_45ba5ed4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _App_vue_vue_type_template_id_45ba5ed4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "45ba5ed4",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/App.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/App.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/App.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/App.vue?vue&type=style&index=0&id=45ba5ed4&scoped=true&lang=css&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_id_45ba5ed4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/nativescript-dev-webpack/style-hot-loader.js!../node_modules/nativescript-dev-webpack/apply-css-loader.js!../node_modules/css-loader/index.js?!../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../node_modules/vue-loader/lib/index.js?!./components/App.vue?vue&type=style&index=0&id=45ba5ed4&scoped=true&lang=css&");
/* harmony import */ var _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_id_45ba5ed4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_id_45ba5ed4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_id_45ba5ed4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_id_45ba5ed4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_id_45ba5ed4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./components/App.vue?vue&type=template&id=45ba5ed4&scoped=true&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_45ba5ed4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/App.vue?vue&type=template&id=45ba5ed4&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_45ba5ed4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_45ba5ed4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/Card.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Card_vue_vue_type_template_id_f883d62a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/Card.vue?vue&type=template&id=f883d62a&scoped=true&");
/* harmony import */ var _Card_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/Card.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Card_vue_vue_type_style_index_0_id_f883d62a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./components/Card.vue?vue&type=style&index=0&id=f883d62a&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Card_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Card_vue_vue_type_template_id_f883d62a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Card_vue_vue_type_template_id_f883d62a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "f883d62a",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/Card.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/Card.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/Card.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/Card.vue?vue&type=style&index=0&id=f883d62a&scoped=true&lang=css&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_style_index_0_id_f883d62a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/nativescript-dev-webpack/style-hot-loader.js!../node_modules/nativescript-dev-webpack/apply-css-loader.js!../node_modules/css-loader/index.js?!../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../node_modules/vue-loader/lib/index.js?!./components/Card.vue?vue&type=style&index=0&id=f883d62a&scoped=true&lang=css&");
/* harmony import */ var _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_style_index_0_id_f883d62a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_style_index_0_id_f883d62a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_style_index_0_id_f883d62a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_style_index_0_id_f883d62a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_style_index_0_id_f883d62a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./components/Card.vue?vue&type=template&id=f883d62a&scoped=true&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_template_id_f883d62a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/Card.vue?vue&type=template&id=f883d62a&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_template_id_f883d62a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Card_vue_vue_type_template_id_f883d62a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/CityOverview.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CityOverview_vue_vue_type_template_id_04915cdf_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/CityOverview.vue?vue&type=template&id=04915cdf&scoped=true&");
/* harmony import */ var _CityOverview_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/CityOverview.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _CityOverview_vue_vue_type_style_index_0_id_04915cdf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./components/CityOverview.vue?vue&type=style&index=0&id=04915cdf&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _CityOverview_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CityOverview_vue_vue_type_template_id_04915cdf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CityOverview_vue_vue_type_template_id_04915cdf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "04915cdf",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/CityOverview.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/CityOverview.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_CityOverview_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/CityOverview.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_CityOverview_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/CityOverview.vue?vue&type=style&index=0&id=04915cdf&scoped=true&lang=css&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_CityOverview_vue_vue_type_style_index_0_id_04915cdf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/nativescript-dev-webpack/style-hot-loader.js!../node_modules/nativescript-dev-webpack/apply-css-loader.js!../node_modules/css-loader/index.js?!../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../node_modules/vue-loader/lib/index.js?!./components/CityOverview.vue?vue&type=style&index=0&id=04915cdf&scoped=true&lang=css&");
/* harmony import */ var _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_CityOverview_vue_vue_type_style_index_0_id_04915cdf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_CityOverview_vue_vue_type_style_index_0_id_04915cdf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_CityOverview_vue_vue_type_style_index_0_id_04915cdf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_CityOverview_vue_vue_type_style_index_0_id_04915cdf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_CityOverview_vue_vue_type_style_index_0_id_04915cdf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./components/CityOverview.vue?vue&type=template&id=04915cdf&scoped=true&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CityOverview_vue_vue_type_template_id_04915cdf_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/CityOverview.vue?vue&type=template&id=04915cdf&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CityOverview_vue_vue_type_template_id_04915cdf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CityOverview_vue_vue_type_template_id_04915cdf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/Header.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Header_vue_vue_type_template_id_1a9bb128_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/Header.vue?vue&type=template&id=1a9bb128&scoped=true&");
/* harmony import */ var _Header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/Header.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Header_vue_vue_type_style_index_0_id_1a9bb128_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./components/Header.vue?vue&type=style&index=0&id=1a9bb128&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Header_vue_vue_type_template_id_1a9bb128_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Header_vue_vue_type_template_id_1a9bb128_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "1a9bb128",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/Header.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/Header.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/Header.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/Header.vue?vue&type=style&index=0&id=1a9bb128&scoped=true&lang=css&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_id_1a9bb128_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/nativescript-dev-webpack/style-hot-loader.js!../node_modules/nativescript-dev-webpack/apply-css-loader.js!../node_modules/css-loader/index.js?!../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../node_modules/vue-loader/lib/index.js?!./components/Header.vue?vue&type=style&index=0&id=1a9bb128&scoped=true&lang=css&");
/* harmony import */ var _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_id_1a9bb128_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_id_1a9bb128_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_id_1a9bb128_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_id_1a9bb128_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_id_1a9bb128_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./components/Header.vue?vue&type=template&id=1a9bb128&scoped=true&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_template_id_1a9bb128_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/Header.vue?vue&type=template&id=1a9bb128&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_template_id_1a9bb128_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_template_id_1a9bb128_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/Home.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Home_vue_vue_type_template_id_67410f3a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/Home.vue?vue&type=template&id=67410f3a&scoped=true&");
/* harmony import */ var _Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/Home.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Home_vue_vue_type_style_index_0_id_67410f3a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./components/Home.vue?vue&type=style&index=0&id=67410f3a&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Home_vue_vue_type_template_id_67410f3a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Home_vue_vue_type_template_id_67410f3a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "67410f3a",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/Home.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/Home.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/Home.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/Home.vue?vue&type=style&index=0&id=67410f3a&scoped=true&lang=css&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_id_67410f3a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/nativescript-dev-webpack/style-hot-loader.js!../node_modules/nativescript-dev-webpack/apply-css-loader.js!../node_modules/css-loader/index.js?!../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../node_modules/vue-loader/lib/index.js?!./components/Home.vue?vue&type=style&index=0&id=67410f3a&scoped=true&lang=css&");
/* harmony import */ var _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_id_67410f3a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_id_67410f3a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_id_67410f3a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_id_67410f3a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_id_67410f3a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./components/Home.vue?vue&type=template&id=67410f3a&scoped=true&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_67410f3a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/Home.vue?vue&type=template&id=67410f3a&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_67410f3a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_67410f3a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/Place.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Place_vue_vue_type_template_id_7012a93c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/Place.vue?vue&type=template&id=7012a93c&scoped=true&");
/* harmony import */ var _Place_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/Place.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Place_vue_vue_type_style_index_0_id_7012a93c_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./components/Place.vue?vue&type=style&index=0&id=7012a93c&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Place_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Place_vue_vue_type_template_id_7012a93c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Place_vue_vue_type_template_id_7012a93c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "7012a93c",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/Place.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/Place.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Place_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/Place.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Place_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/Place.vue?vue&type=style&index=0&id=7012a93c&scoped=true&lang=css&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Place_vue_vue_type_style_index_0_id_7012a93c_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/nativescript-dev-webpack/style-hot-loader.js!../node_modules/nativescript-dev-webpack/apply-css-loader.js!../node_modules/css-loader/index.js?!../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../node_modules/vue-loader/lib/index.js?!./components/Place.vue?vue&type=style&index=0&id=7012a93c&scoped=true&lang=css&");
/* harmony import */ var _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Place_vue_vue_type_style_index_0_id_7012a93c_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Place_vue_vue_type_style_index_0_id_7012a93c_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Place_vue_vue_type_style_index_0_id_7012a93c_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Place_vue_vue_type_style_index_0_id_7012a93c_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Place_vue_vue_type_style_index_0_id_7012a93c_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./components/Place.vue?vue&type=template&id=7012a93c&scoped=true&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Place_vue_vue_type_template_id_7012a93c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/Place.vue?vue&type=template&id=7012a93c&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Place_vue_vue_type_template_id_7012a93c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Place_vue_vue_type_template_id_7012a93c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./components/Searching.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Searching_vue_vue_type_template_id_1a2bcd22_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/Searching.vue?vue&type=template&id=1a2bcd22&scoped=true&");
/* harmony import */ var _Searching_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/Searching.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Searching_vue_vue_type_style_index_0_id_1a2bcd22_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./components/Searching.vue?vue&type=style&index=0&id=1a2bcd22&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Searching_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Searching_vue_vue_type_template_id_1a2bcd22_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Searching_vue_vue_type_template_id_1a2bcd22_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "1a2bcd22",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/Searching.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/Searching.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Searching_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/Searching.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Searching_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/Searching.vue?vue&type=style&index=0&id=1a2bcd22&scoped=true&lang=css&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Searching_vue_vue_type_style_index_0_id_1a2bcd22_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/nativescript-dev-webpack/style-hot-loader.js!../node_modules/nativescript-dev-webpack/apply-css-loader.js!../node_modules/css-loader/index.js?!../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../node_modules/vue-loader/lib/index.js?!./components/Searching.vue?vue&type=style&index=0&id=1a2bcd22&scoped=true&lang=css&");
/* harmony import */ var _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Searching_vue_vue_type_style_index_0_id_1a2bcd22_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Searching_vue_vue_type_style_index_0_id_1a2bcd22_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Searching_vue_vue_type_style_index_0_id_1a2bcd22_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Searching_vue_vue_type_style_index_0_id_1a2bcd22_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_Searching_vue_vue_type_style_index_0_id_1a2bcd22_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./components/Searching.vue?vue&type=template&id=1a2bcd22&scoped=true&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Searching_vue_vue_type_template_id_1a2bcd22_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/Searching.vue?vue&type=template&id=1a2bcd22&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Searching_vue_vue_type_template_id_1a2bcd22_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Searching_vue_vue_type_template_id_1a2bcd22_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./nativescript-dom/dom.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/**********************************************************************************
 * (c) 2015-2017, Master Technology
 * Licensed under the MIT license or contact me for a Support or Commercial License
 *
 * I do contract work in most languages, so let me solve your problems!
 *
 * Any questions please feel free to email me or put a issue up on the github repo
 * Version 0.1.0                                      Nathan@master-technology.com
 *********************************************************************************/

/* jshint node: true, browser: true, unused: true, undef: true */

/* global global */
// Load the required modules

var viewBase = __webpack_require__("../node_modules/tns-core-modules/ui/core/view-base/view-base.js");

var frame = __webpack_require__("../node_modules/tns-core-modules/ui/frame/frame.js"); // global.android is already defined on android devices
// We are defining global.ios on ios devices, since the iOS team can't seem to do it.  ;-)


if (global.NSObject && global.NSString && typeof global.ios === "undefined") {
  global.ios = true;
  Object.freeze(global.ios);
}

if (!global.getElementById) {
  /***
   * Find a element by an id
   * @param id
   * @returns {ViewBase} or {undefined}
   */
  global.getElementById = function (id) {
    return getElementById(getCurrentActiveModel(), id);
  };
}

if (!viewBase.ViewBase.prototype.getElementById) {
  /***
   * Find an element by a id
   * @param id
   * @returns {ViewBase} or {undefined}
   */
  viewBase.ViewBase.prototype.getElementById = function (id) {
    return getElementById(this, id);
  };
}

if (!global.getElementsByClassName) {
  /***
   * getElementsByClassName
   * @param className - The class name
   * @returns {Array} of elements
   */
  global.getElementsByClassName = function (className) {
    return getElementsByClassName(getCurrentActiveModel(), className);
  };
}

if (!viewBase.ViewBase.prototype.getElementsByClassName) {
  /***
   * Finds all elements with the class name
   * @param className - the Class name
   * @returns {Array} of elements
   */
  viewBase.ViewBase.prototype.getElementsByClassName = function (className) {
    return getElementsByClassName(this, className);
  };
}

if (!global.getElementsByTagName) {
  /**
   * Finds all elements by a Tag name
   * @param tagName
   * @returns {Array}
   */
  global.getElementsByTagName = function (tagName) {
    return getElementsByTagName(getCurrentActiveModel(), tagName);
  };
}

if (!viewBase.ViewBase.prototype.getElementsByTagName) {
  /**
   * Finds all elements by a Tag name
   * @param tagName
   * @returns {Array}
   */
  viewBase.ViewBase.prototype.getElementsByTagName = function (tagName) {
    return getElementsByTagName(this, tagName);
  };
}

if (!viewBase.ViewBase.prototype.classList) {
  var classList = function classList(t) {
    var curClassList = "";

    this._resync = function () {
      if (curClassList === t.className) {
        return;
      } // We need to zero our length; so that we can re-add anything that exists in the parent class


      this.length = 0;
      var self = this;
      t.cssClasses.forEach(function (item) {
        self.push(item);
      });
    };

    this._update = function () {
      curClassList = this.join(" ");
      t.className = curClassList;
    };

    this._resync();
  };

  classList.prototype = [];

  classList.prototype.toString = function () {
    this._resync();

    return this.join(" ");
  };

  classList.prototype.item = function (i) {
    this._resync();

    return this[i] || null;
  };

  classList.prototype.add = function () {
    this._resync();

    var updated = false;

    for (var i = 0, len = arguments.length; i < len; i++) {
      if (!this.contains(arguments[i])) {
        this.push(arguments[i]);
        updated = true;
      }
    }

    if (updated) {
      this._update();
    }

    return this;
  };

  classList.prototype.insert = function () {
    this._resync();

    var updated = false;

    for (var i = 0, len = arguments.length; i < len; i++) {
      if (!this.contains(arguments[i])) {
        this.unshift(arguments[i]);
        updated = true;
      }
    }

    if (updated) {
      this._update();
    }

    return this;
  };

  classList.prototype.remove = function () {
    this._resync();

    var updated = false;

    for (var i = 0, len = arguments.length; i < len; i++) {
      var idx = this.indexOf(arguments[i]);

      if (idx >= 0) {
        this.splice(idx, 1);
        updated = true;
      }
    }

    if (updated) {
      this._update();
    }

    return this;
  };

  classList.prototype.toggle = function (val, force) {
    this._resync();

    if (this.contains(val)) {
      if (force === true) {
        return this;
      }

      return this.remove(val);
    } else {
      if (force === false) {
        return this;
      }

      return this.add(val);
    }
  };

  classList.prototype.contains = function (c) {
    return this.indexOf(c) >= 0;
  };

  var getClassList = function getClassList(val) {
    var cl = new classList(val);
    Object.defineProperty(val, "classList", {
      value: cl,
      configurable: true,
      enumerable: true
    });
    return cl;
  };

  Object.defineProperty(viewBase.ViewBase.prototype, "classList", {
    configurable: true,
    enumerable: true,
    get: function get() {
      return getClassList(this);
    }
  });
}

global.runAgainstClasses = function (clsName, func) {
  runAgainstClasses(getCurrentActiveModel(), clsName, func);
};

viewBase.ViewBase.prototype.runAgainstClasses = function (clsName, func) {
  runAgainstClasses(this, clsName, func);
};

global.runAgainstTagNames = function (tagName, func) {
  runAgainstTagNames(getCurrentActiveModel(), tagName, func);
};

viewBase.ViewBase.prototype.runAgainstTagNames = function (tagName, func) {
  runAgainstTagNames(this, tagName, func);
};

global.runAgainstId = function (id, func) {
  runAgainstId(getCurrentActiveModel(), id, func);
};

viewBase.ViewBase.prototype.runAgainstId = function (id, func) {
  runAgainstId(this, id, func);
};
/*** Support routines, not publicly accessible ***/


function getElementById(v, id) {
  if (!v) {
    return undefined;
  }

  if (v.id === id) {
    return view;
  }

  var retVal = undefined;

  var viewCallBack = function viewCallBack(child) {
    if (child.id === id) {
      retVal = child;
      return false;
    } // Android patch for ListView


    if (child._realizedItems && child._realizedItems.size !== child._childrenCount) {
      for (var key in child._realizedItems) {
        if (child._realizedItems.hasOwnProperty(key)) {
          // We return false, when we have a hit; so if we have a hit we can stop searching
          if (!viewCallBack(child._realizedItems[key])) {
            return false;
          }
        }
      }
    }

    return true;
  };

  viewBase.eachDescendant(v, viewCallBack);

  if (typeof retVal === "undefined") {
    // Android patch for ListView
    if (v._realizedItems && v._realizedItems.size !== v._childrenCount) {
      for (var key in v._realizedItems) {
        if (v._realizedItems.hasOwnProperty(key)) {
          // viewCallback will return false, if we found a match
          if (!viewCallBack(v._realizedItems[key])) {
            return retVal;
          }
        }
      }
    }
  }

  return retVal;
}

function getElementsByClassName(v, clsName) {
  var retVal = [];

  if (!v) {
    return retVal;
  }

  if (v.classList.contains(clsName)) {
    retVal.push(v);
  }

  var classNameCallback = function classNameCallback(child) {
    if (child.classList.contains(clsName)) {
      retVal.push(child);
    } // Android patch for ListView


    if (child._realizedItems && child._realizedItems.size !== child._childrenCount) {
      for (var key in child._realizedItems) {
        if (child._realizedItems.hasOwnProperty(key)) {
          classNameCallback(child._realizedItems[key]);
        }
      }
    }

    return true;
  };

  viewBase.eachDescendant(v, classNameCallback); // Android patch for ListView

  if (v._realizedItems && v._realizedItems.size !== v._childrenCount) {
    for (var key in v._realizedItems) {
      if (v._realizedItems.hasOwnProperty(key)) {
        classNameCallback(v._realizedItems[key]);
      }
    }
  }

  return retVal;
}

function getElementsByTagName(v, tagName) {
  // TagName is case-Insensitive
  var tagNameLC = tagName.toLowerCase();
  var retVal = [],
      allTags = false;

  if (!v) {
    return retVal;
  }

  if (tagName === '*') {
    allTags = true;
  }

  if (v.typeName && v.typeName.toLowerCase() === tagNameLC || allTags) {
    retVal.push(v);
  }

  var tagNameCallback = function tagNameCallback(child) {
    if (child.typeName && child.typeName.toLowerCase() === tagNameLC || allTags) {
      retVal.push(child);
    } // Android patch for ListView


    if (child._realizedItems && child._realizedItems.size !== child._childrenCount) {
      for (var key in child._realizedItems) {
        if (child._realizedItems.hasOwnProperty(key)) {
          tagNameCallback(child._realizedItems[key]);
        }
      }
    }

    return true;
  };

  viewBase.eachDescendant(v, tagNameCallback); // Android patch for ListView

  if (v._realizedItems && v._realizedItems.size !== v._childrenCount) {
    for (var key in v._realizedItems) {
      if (v._realizedItems.hasOwnProperty(key)) {
        tagNameCallback(v._realizedItems[key]);
      }
    }
  }

  return retVal;
}

var getCurrentActiveModel = function getCurrentActiveModel() {
  var topFrame = frame.topmost();
  var model = topFrame.currentPage && topFrame.currentPage.model;

  if (model) {
    return model;
  }

  return topFrame;
};

function runAgainstClasses(v, clsName, func) {
  var elements = getElementsByClassName(v, clsName);

  for (var i = 0; i < elements.length; i++) {
    func(elements[i]);
  }
}

function runAgainstTagNames(v, tagName, func) {
  var elements = getElementsByTagName(v, tagName);

  for (var i = 0; i < elements.length; i++) {
    func(elements[i]);
  }
}

function runAgainstId(v, id, func) {
  var element = getElementById(v, id);

  if (element) {
    func(element);
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/nativescript-dev-webpack/node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./nativescript-globalevents/globalevents.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/**********************************************************************************
 * (c) 2016-2017, Master Technology
 * Licensed under the MIT license or contact me for a Support or Commercial License
 *
 * I do contract work in most languages, so let me solve your problems!
 *
 * Any questions please feel free to email me or put a issue up on the github repo
 * Version 1.2.1                                      Nathan@master-technology.com
 *********************************************************************************/

/* jshint camelcase: false, nodejs: true */

/* global android */

var page = __webpack_require__("../node_modules/tns-core-modules/ui/page/page.js");

var View = __webpack_require__("../node_modules/tns-core-modules/ui/core/view/view.js").View; // global.android is already defined on android devices
// We are defining global.ios on ios devices because the ios team refuses to do it


if (global.NSObject && global.NSString && typeof global.ios === "undefined") {
  global.ios = true;
  Object.freeze(global.ios);
} // If NS has added this ability to core; we want our plugin to gracefully stop working without doing anything...


if (page.Page.on || page.Page.addEventListener) {
  console.log("NativeScript-globalevents auto disabled; functionality appears to be already present!");
} else {
  var createdPageEvent = "pageCreated";
  var loadedFirst = View.loadedEvent + "First";
  var unloadedFirst = View.unloadedEvent + "First";
  var navigatingToFirst = page.Page.navigatingToEvent + "First";
  var navigatingFromFirst = page.Page.navigatingFromEvent + "First";
  var navigatedToFirst = page.Page.navigatedToEvent + "First";
  var navigatedFromFirst = page.Page.navigatedFromEvent + "First";
  var shownModallyFirst = page.Page.shownModallyEvent + "First";
  var showingModallyFirst = page.Page.showingModallyEvent + "First"; // Setup the original events tracking

  var events = {}; // Setup our event trackers

  var eventHandlers = {};
  eventHandlers[page.Page.navigatingToEvent] = [];
  eventHandlers[page.Page.navigatedToEvent] = [];
  eventHandlers[page.Page.navigatingFromEvent] = [];
  eventHandlers[page.Page.navigatedFromEvent] = [];
  eventHandlers[page.Page.shownModallyEvent] = [];
  eventHandlers[page.Page.showingModallyEvent] = [];
  eventHandlers[View.loadedEvent] = [];
  eventHandlers[View.unloadedEvent] = [];
  eventHandlers[createdPageEvent] = [];
  eventHandlers[loadedFirst] = [];
  eventHandlers[unloadedFirst] = [];
  eventHandlers[navigatingToFirst] = [];
  eventHandlers[navigatingFromFirst] = [];
  eventHandlers[navigatedToFirst] = [];
  eventHandlers[navigatedFromFirst] = [];
  eventHandlers[showingModallyFirst] = [];
  eventHandlers[shownModallyFirst] = []; // The event handler names

  var eventNames = {};
  eventNames[page.Page.navigatedToEvent] = 'onNavigatedTo';
  eventNames[page.Page.navigatingToEvent] = 'onNavigatingTo';
  eventNames[page.Page.navigatedFromEvent] = 'onNavigatedFrom';
  eventNames[page.Page.navigatingFromEvent] = 'onNavigatingFrom';
  eventNames[page.Page.shownModallyEvent] = '_raiseShownModallyEvent';
  eventNames[page.Page.showingModallyEvent] = '_raiseShowingModallyEvent';
  eventNames[View.loadedEvent] = 'onLoaded';
  eventNames[View.unloadedEvent] = 'onUnloaded';
  eventNames[createdPageEvent] = createdPageEvent;
  eventNames[loadedFirst] = 'onLoaded';
  eventNames[unloadedFirst] = 'onUnloaded';
  eventNames[navigatingToFirst] = 'onNavigatingTo';
  eventNames[navigatingFromFirst] = 'onNavigatingFrom';
  eventNames[navigatedToFirst] = 'onNavigatedTo';
  eventNames[navigatedFromFirst] = 'onNavigatedFrom';
  eventNames[shownModallyFirst] = '_raiseShownModallyEvent';
  eventNames[showingModallyFirst] = '_raiseShowingModallyEvent';
  /** Add our newly Created Page Event **/

  page.Page.createdPageEvent = createdPageEvent;
  page.Page.loadedFirst = loadedFirst;
  page.Page.unloadedFirst = unloadedFirst;
  page.Page.navigatedFromFirst = navigatedFromFirst;
  page.Page.navigatingFromFirst = navigatingFromFirst;
  page.Page.navigatedToFirst = navigatedToFirst;
  page.Page.navigatingToFirst = navigatingToFirst;
  page.Page.showingModallyFirst = showingModallyFirst;
  page.Page.shownModallyFirst = shownModallyFirst; // Overwrite the Page class with our Page class.

  var Page = function (_super) {
    __extends(Page, _super);

    function Page() {
      var _this = _super !== null && _super.apply(this, arguments) || this;

      runEvent(Page.createdPageEvent, {
        object: this
      });
      return _this;
    }
    /**
     * Setup a static on/addEventLister on the Page object
     * @type {Page.addEventListener}
     */


    Page.on = Page.addEventListener = function (event, callback, thisArg) {
      if (typeof eventHandlers[event] === 'undefined') {
        throw new Error("This global page event " + event + " does not exist, or is currently unsupported");
      }

      if (typeof callback !== 'function') {
        throw new Error("Callback should be a function!");
      }

      var lookupEvent;

      if (event.indexOf("First") !== -1) {
        lookupEvent = event.substring(0, event.length - 5);
      } else {
        lookupEvent = event;
      } // Dynamically hijack the event; so that we don't bother hijacking any events that aren't used


      if (eventHandlers[event].length === 0) {
        hijackEvent(lookupEvent);
      }

      eventHandlers[event].push({
        callback: callback,
        thisArg: thisArg
      });
    };
    /**
     * Setup a static off/removeEventLister
     * @type {Page.removeEventListener}
     */


    Page.off = Page.removeEventListener = function (event, callback, thisArg) {
      if (typeof eventHandlers[event] === "undefined") {
        throw new Error("This global page event " + event + " does not exist.");
      }

      var orgLength = eventHandlers[event].length;

      if (callback) {
        var index = indexOfListener(eventHandlers[event], callback, thisArg);

        if (index >= 0) {
          eventHandlers[event].splice(index, 1);
        }
      } else {
        eventHandlers[event] = [];
      }

      if (eventHandlers[event].length === 0 && orgLength !== 0) {
        restoreEvent(event);
      }
    };

    return Page;
  }(page.Page); // In case they include globalevents AFTER they have the Page variable; we need to
  // Make sure the original Page variable also has these events.


  page.Page.on = page.Page.addEventListener = Page.on;
  page.Page.off = page.Page.removeEventListener = Page.off; // If NativeScript-Angular has been loaded; this will be defined

  if (typeof global.Zone === "undefined") {
    // Overwrite the Page variable with our class only in a PAN or VUE app
    page.Page = Page;
  } else {
    // Because we are in Angular; we will grab the "Default" page class and
    // use it, instead of our new Page class.  Basically we "forget" about our
    // new page class in Angular mode.
    Page = page.Page;
  }
}
/**
 * Used to dynamically hijack the event
 * @param event
 */


function hijackEvent(event) {
  // We don't need to hijack the pageCreated event
  if (event === createdPageEvent) {
    return;
  } // Event has already been hijacked, no need to do anything more


  if (events[event]) {
    return;
  }

  var handlerName = eventNames[event];

  if (typeof Page.prototype[handlerName] === 'function') {
    events[event] = Page.prototype[handlerName];
  } else if (typeof View.prototype[handlerName] === 'function') {
    events[event] = View.prototype[handlerName];
  } else {
    throw new Error("NativeScript-globalEvents is unable to find the proper event to attach to.  Please alert the author!");
  }

  Page.prototype[handlerName] = getEventHandler(event);
}
/**
 * Used to dynamically restore the event
 * @param event
 */


function restoreEvent(event) {
  // We don't need to restore the pageCreated event
  if (event === createdPageEvent) {
    return;
  } // Get handler name


  var handlerName = eventNames[event];

  if (typeof events[event] !== 'function') {
    throw new Error("NativeScript-globalEvent, this event was never hijacked to be removed!");
  }

  Page.prototype[handlerName] = events[event];
  events[event] = null;
}
/**
 * Find the Event in the callback list and return its index
 * @param events
 * @param callback
 * @param thisArg
 * @returns {number}
 */


function indexOfListener(events, callback, thisArg) {
  var i;

  if (thisArg) {
    for (i = 0; i < events.length; i++) {
      if (events[i].callback === callback && events[i].thisArg === thisArg) {
        return i;
      }
    }
  } else {
    for (i = 0; i < events.length; i++) {
      if (events[i].callback === callback) {
        return i;
      }
    }
  }

  return -1;
}

function runEvent(eventType, eventArgs) {
  var eh = eventHandlers[eventType];

  if (typeof eh !== 'undefined' && eh.length) {
    for (var i = 0; i < eh.length; i++) {
      var thisArg = eh[i].thisArg || this;
      eh[i].callback.call(thisArg, eventArgs);
    }
  }
}
/**
 * Setups a custom event handler for usage
 * @param eventType
 * @returns {Function}
 */


function getEventHandler(eventType) {
  return function (arg1, arg2) {
    var eventArgs;

    switch (eventType) {
      case Page.navigatingToEvent:
        eventArgs = this.createNavigatedData(Page.navigatingToEvent, arg2);
        break;

      case Page.shownModallyEvent:
        eventArgs = {
          eventName: Page.shownModallyEvent,
          object: this,
          context: arg2
        };
        break;

      case Page.showingModallyEvent:
        eventArgs = {
          eventName: Page.showingModallyEvent,
          object: this
        };
        break;

      case View.unloadedEvent:
      case View.loadedEvent:
        eventArgs = {
          eventName: eventType,
          object: this
        };
        break;

      default:
        eventArgs = this.createNavigatedData(eventType, arg1);
    } // Run the "First" Events


    runEvent(eventType + "First", eventArgs); // Run real original event

    events[eventType].apply(this, arguments); // Run the normal "post" events

    runEvent(eventType, eventArgs);
  };
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/nativescript-dev-webpack/node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./nativescript-orientation/orientation.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/**********************************************************************************
 * (c) 2016-2018, Master Technology
 * Licensed under the MIT license or contact me for a Support or Commercial License
 *
 * I do contract work in most languages, so let me solve your problems!
 *
 * Any questions please feel free to email me or put a issue up on the github repo
 * Version 2.0.1                                      Nathan@master-technology.com
 *********************************************************************************/

/* jshint camelcase: false */

/* global UIDevice, UIDeviceOrientation, UIView, getElementsByTagName, android */

var application = __webpack_require__("../node_modules/tns-core-modules/application/application.js");

var view = __webpack_require__("../node_modules/tns-core-modules/ui/core/view/view.js");

var enums = __webpack_require__("../node_modules/tns-core-modules/ui/enums/enums.js");

var frame = __webpack_require__("../node_modules/tns-core-modules/ui/frame/frame.js");

var Page = __webpack_require__("../node_modules/tns-core-modules/ui/page/page.js").Page;

var utils = __webpack_require__("../node_modules/tns-core-modules/utils/utils.js"); // Load the helper plugins


__webpack_require__("./nativescript-globalevents/globalevents.js");

__webpack_require__("./nativescript-dom/dom.js");

var allowRotation = true,
    forceRotation = false,
    fullScreen = false,
    abHidden = false;
var orientation = {};
var orientationAppliers = [];
module.exports = orientation;
/**
 * Function to add a new orientation applier callback
 */

orientation.addOrientationApplier = function (newOrientationApplier) {
  if (!newOrientationApplier) return;
  var existingApplier = orientationAppliers.find(function (oa) {
    return oa === newOrientationApplier;
  });
  if (existingApplier) return;
  orientationAppliers.push(newOrientationApplier);
};
/**
 * Function to remove an orientation applier callback
 */


orientation.removeOrientationApplier = function (orientationApplier) {
  if (!orientationApplier) return;
  orientationAppliers = orientationAppliers.filter(function (oa) {
    return oa !== orientationApplier;
  });
};
/**
 * Helper function hooked to the Application to get the current orientation
 */


if (global.android) {
  orientation.getOrientation = function () {
    var context = getContext(); // These both lie on Tablets, The orientation is opposite, so it is safer to use .orientation value
    // context.getSystemService("window").getDefaultDisplay().getOrientation();
    // context.getSystemService("window").getDefaultDisplay().getRotation();

    var currentOrientation = getContext().getResources().getConfiguration().orientation;

    switch (currentOrientation) {
      case 2:
        /* LANDSCAPE */
        return enums.DeviceOrientation.landscape;

      case 1:
        /* PORTRAIT */
        return enums.DeviceOrientation.portrait;

      default:
        break;
    } // If for some reason the orientation code above doesn't figure it out; we use sizes


    var metrics = new android.util.DisplayMetrics();
    context.getSystemService(android.content.Context.WINDOW_SERVICE).getDefaultDisplay().getRealMetrics(metrics);

    if (metrics.widthPixels > metrics.heightPixels) {
      return enums.DeviceOrientation.landscape;
    }

    return enums.DeviceOrientation.portrait;
  };

  orientation.enableRotation = function () {
    if (!application.android || !application.android.foregroundActivity) {
      setTimeout(orientation.enableRotation, 100);
      return;
    }

    var activity = application.android.foregroundActivity;
    activity.setRequestedOrientation(13); // SCREEN_ORIENTATION_FULL_USER = 13
  };

  orientation.disableRotation = function () {
    if (!application.android || !application.android.foregroundActivity) {
      setTimeout(orientation.disableRotation, 100);
      return;
    }

    var activity = application.android.foregroundActivity;
    activity.setRequestedOrientation(14); // SCREEN_ORIENTATION_LOCKED = 14
  };

  orientation.setOrientation = function (value, animation) {
    if (!application.android || !application.android.foregroundActivity) {
      setTimeout(function () {
        orientation.setOrientation(value, animation);
      }, 100);
      return;
    }

    var activity = application.android.foregroundActivity;
    var val = value.toLowerCase();
    var newOrientation;

    switch (val) {
      case 'landscape':
        newOrientation = 6; // SCREEN_ORIENTATION_SENSOR_LANDSCAPE = 6

        break;

      case 'landscaperight':
        newOrientation = 0; // SCREEN_ORIENTATION_LANDSCAPE = 0

        break;

      case 'landscapeleft':
        newOrientation = 8; // SCREEN_ORIENTATION_REVERSE_LANDSCAPE = 9

        break;

      case 'portrait':
      default:
        newOrientation = 1; // SCREEN_ORIENTATION_PORTRAIT = 1

        break;
    }

    activity.setRequestedOrientation(newOrientation); // Animation: https://developer.android.com/reference/android/view/WindowManager.LayoutParams.html#ROTATION_ANIMATION_JUMPCUT
    // and https://developer.android.com/reference/android/view/WindowManager.LayoutParams.html#rotationAnimation
  };

  orientation.setFullScreen = function (value) {
    var View = android.view.View;
    var WindowManager = android.view.WindowManager;
    var window = application.android.startActivity.getWindow();
    fullScreen = !!value;

    if (fullScreen) {
      window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
      window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    } else {
      window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
      window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
    }
    /*
    			if (fullScreen) {
    				if (!frame.topmost().currentPage.actionBarHidden && !abHidden) {
    					abHidden = true;
    					frame.topmost().currentPage.actionBarHidden = true;
    				}
    			} else if (abHidden) {
    				abHidden = false;
    				frame.topmost().currentPage.actionBarHidden = false;
    			}
    */

  };
} else if (global.NSObject && global.UIDevice) {
  setupiOSController();

  orientation.getOrientation = function () {
    var device = utils.ios.getter(UIDevice, UIDevice.currentDevice);

    switch (device.orientation) {
      case UIDeviceOrientation.UIDeviceOrientationLandscapeRight:
      case UIDeviceOrientation.UIDeviceOrientationLandscapeLeft:
        return enums.DeviceOrientation.landscape;

      case UIDeviceOrientation.UIDeviceOrientationPortraitUpsideDown:
      case UIDeviceOrientation.UIDeviceOrientationPortrait:
        return enums.DeviceOrientation.portrait;

      default:
        // Since we have a up/Down orientation, we need to see what the statusbar is set to to get the actual current device orientation
        var appOrientation = utils.ios.getter(UIApplication, UIApplication.sharedApplication).statusBarOrientation;

        if (appOrientation === 1 || appOrientation === 2) {
          return enums.DeviceOrientation.portrait;
        } else {
          return enums.DeviceOrientation.landscape;
        }

    }
  };

  orientation.setOrientation = function (value, animation) {
    var newOrientation,
        val = value.toLowerCase();

    if (val === 'landscape' || val === 'landscaperight') {
      newOrientation = NSNumber.numberWithInt(UIInterfaceOrientationLandscapeRight);
    } else if (val === 'landscapeleft') {
      newOrientation = NSNumber.numberWithInt(UIInterfaceOrientationLandscapeLeft);
    } else {
      newOrientation = NSNumber.numberWithInt(UIInterfaceOrientationPortrait);
    }

    var device = utils.ios.getter(UIDevice, UIDevice.currentDevice);

    if (animation === false) {
      UIView.setAnimationsEnabled(false);
    }

    allowRotation = false; // disable rotations...

    forceRotation = true;
    var currentOrientation = device.orientation; // We have to swap to a different orientation FIRST, if the current orientation matches

    if (newOrientation === currentOrientation) {
      var tempOrientation = newOrientation - 1;

      if (tempOrientation < 1) {
        tempOrientation += 2;
      }

      device.setValueForKey(tempOrientation, "orientation");
    }

    device.setValueForKey(newOrientation, "orientation");
    forceRotation = false;

    if (animation === false) {
      UIView.setAnimationsEnabled(true);
    }
  };

  orientation.enableRotation = function () {
    allowRotation = true;
  };

  orientation.disableRotation = function () {
    allowRotation = false;
  };

  orientation.setFullScreen = function (setFullScreen) {
    fullScreen = !!setFullScreen;
    var app = iosProperty(UIApplication, UIApplication.sharedApplication);
    app.setStatusBarHiddenWithAnimation(fullScreen, UIStatusBarAnimation.Slide);
  };

  var resetLandscapedLock = false;
  application.on('suspend', function () {
    if (allowRotation === false && orientation.getOrientation() === 'landscape') {
      allowRotation = true;
      resetLandscapedLock = true;
    }
  });
  application.on('resume', function () {
    if (resetLandscapedLock) {
      resetLandscapedLock = false;
      orientation.setOrientation('landscape', false);
    }
  });
} // Depreciated; but supported for backwards compatibility


application.getOrientation = orientation.getOrientation;
/**
 * Searchs for a prototype in the prototype chain
 * @param source - Source element
 * @param name - the name of the element
 * @returns {*}
 */

function findRootPrototype(source, name) {
  var proto = source;

  do {
    proto = Object.getPrototypeOf(proto);
  } while (proto !== null && !proto.hasOwnProperty(name));

  return proto;
}
/**
 * Sets up the iOS Controller configuration
 */


function setupiOSController() {
  var curFrame = frame.topmost();

  if (!curFrame) {
    setTimeout(setupiOSController, 100);
    return;
  }

  try {
    var app = curFrame.ios.controller;
    var proto = findRootPrototype(app, "shouldAutorotate");

    if (proto === null) {
      console.log("Unable to find rotations system, disabling orientation system.");
      return;
    }

    Object.defineProperty(proto, "shouldAutorotate", {
      get: function get() {
        //console.log("Should rotate", forceRotation, allowRotation);
        return forceRotation || allowRotation;
      },
      enumerable: true,
      configurable: true
    });
  } catch (err) {
    console.log("Unable to setup Rotation", err);
  }
}
/**
 * Helper function to look for children that have refresh (i.e. like ListView's) and call their refresh since the
 * CSS changes will probably impact them
 * @param child
 * @returns {boolean}
 */


function resetChildrenRefreshes(child) {
  if (typeof child.refresh === 'function') {
    child.refresh();
  }

  return true;
}
/**
 * Function that does the majority of the work
 * @param page
 * @param args
 */


var applyOrientationToPage = function applyOrientationToPage(page, args) {
  var currentOrientation = orientation.getOrientation(); // If somehow we didn't get the orientation we don't do anything!

  if (!currentOrientation) return; // Check what the current rotation vs the existing page rotation is

  var isLandscape = currentOrientation === enums.DeviceOrientation.landscape;

  if (!args || !args.force) {
    var containsLandScape = page.classList.contains("landscape"); // No need to run the swap if it already has the correct orientation

    if (isLandscape === containsLandScape) {
      return;
    }
  } // Change Orientation


  page.classList.toggle('landscape', isLandscape); // Unfortunately there is a bug in the NS CSS parser, so we have to work around it

  var i;

  if (page.classList.contains('android')) {
    for (i = 0; i < page.classList.length; i++) {
      if (page.classList[i].indexOf('android') === 0) {
        if (page.classList[i].indexOf('.') >= 0) {
          continue;
        }

        page.classList.toggle(page.classList[i] + ".landscape", isLandscape);
      }
    }
  } else if (page.classList.contains('ios')) {
    for (i = 0; i < page.classList.length; i++) {
      if (page.classList[i].indexOf('ios') === 0) {
        if (page.classList[i].indexOf('.') >= 0) {
          continue;
        }

        page.classList.toggle(page.classList[i] + ".landscape", isLandscape);
      }
    }
  } else if (page.classList.contains('windows')) {
    page.classList.toggle('windows.landscape', isLandscape);
  } // --- End NS Bug Patch ---
  // _refreshCSS will verify against the Scope to make sure all the correct styles are
  // applied. If they are already applied, it will short circuit and not
  // do anything more.


  if (typeof page._refreshCss === 'function') {
    // TNS 3.2-
    page._refreshCss();
  } else {
    // TNS 3.3+
    page._onCssStateChange();
  }

  if (args != null) {
    view.eachDescendant(page, resetChildrenRefreshes);
  }

  if (page.exports && typeof page.exports.orientation === "function") {
    page.exports.orientation({
      landscape: isLandscape,
      object: page
    });
  }

  callOrientationAppliers(page, isLandscape);
};
/**
 * This handles an Orientation change event
 * @param args
 */


var handleOrientationChange = function handleOrientationChange(args) {
  // If the topmost frame doesn't exist we can't do anything...
  if (!frame.topmost()) {
    return;
  }

  var currentPage = frame.topmost().currentPage;

  if (currentPage) {
    applyOrientationToPage(currentPage, args);
  }
};

var handleNavigatingTo = function handleNavigatingTo(args) {
  var targetPage = args.object;

  if (targetPage) {
    applyOrientationToPage(targetPage, {
      force: true
    });
  }
};

function callOrientationAppliers(page, isLandscape) {
  if (!orientationAppliers || orientationAppliers.length <= 0) {
    return;
  }

  for (var i = 0; i < orientationAppliers.length; i++) {
    orientationAppliers[i]({
      landscape: isLandscape,
      object: page
    });
  }
}

function getContext() {
  var ctx = java.lang.Class.forName("android.app.AppGlobals").getMethod("getInitialApplication", null).invoke(null, null);

  if (ctx) {
    return ctx;
  }

  return java.lang.Class.forName("android.app.ActivityThread").getMethod("currentApplication", null).invoke(null, null);
}

function iosProperty(theClass, theProperty) {
  if (typeof theProperty === "function") {
    // xCode 7 and below
    return theProperty.call(theClass);
  } else {
    // xCode 8+
    return theProperty;
  }
} // Setup Events


Page.on(Page.navigatingToEvent, handleNavigatingTo);
application.on(application.orientationChangedEvent, handleOrientationChange);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/nativescript-dev-webpack/node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./package.json":
/***/ (function(module) {

module.exports = {"android":{"v8Flags":"--expose_gc","forceLog":true},"main":"app.js","name":"tns-template-vue","version":"3.2.0"};

/***/ })

/******/ });