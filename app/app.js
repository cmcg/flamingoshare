import Vue from 'nativescript-vue';

// Vue.registerElement('CardView', () => require('./nativescript-cardview').CardView);


import App from './components/App';
//import App from './components/CityOverview';
// Uncommment the following to see NativeScript-Vue output logs
Vue.config.silent = false;
import orientation from './nativescript-orientation'

const firebase = require("nativescript-plugin-firebase");

firebase.init({
    // Optionally pass in properties for database, authentication and cloud messaging,
    // see their respective docs.
}).then(
    () => {
        console.log("firebase.init done");
    },
    error => {
        console.log(`firebase.init error: ${error}`);
    }
);


new Vue({

    render: h => h('frame', [h(App)]),
    mounted() {
        setTimeout(() => {

        }, 0)
    }


}).$start();