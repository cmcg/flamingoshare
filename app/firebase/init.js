import firebase from 'firebase'
import firestore from 'firebase/firestore'


var config = {
    apiKey: "AIzaSyDl14uheasfaljyJqyL1wUuh76hlo8NLu8",
    authDomain: "flamingo-video.firebaseapp.com",
    databaseURL: "https://flamingo-video.firebaseio.com",
    projectId: "flamingo-video",
    storageBucket: "flamingo-video.appspot.com",
    messagingSenderId: "527404212394"
};
firebase.initializeApp(config);

const firebaseApp = firebase.initializeApp(config);

// export firestore database
export default firebaseApp.firestore()